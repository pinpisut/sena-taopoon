<?php
require_once('upload_file.php');

class imageupload
{
	public function upload_tmp($folder,$img_content,$filename)
	{
		### get width and height ###
		$ori_w = $this->get_width($img_content);
		$ori_h = $this->get_height($img_content);
		
		### explode file name ###
		$g_img = explode(".",$filename);
		$img_type = strtolower($g_img[1]);
		$file_up = time().".".$img_type;
		
		switch($img_type):
			case "jpg": $src = imagecreatefromjpeg($img_content); break;
			case "jpeg": $src = imagecreatefromjpeg($img_content); break;
			case "png": $src = imagecreatefrompng($img_content); break;
			case "gif": $src = imagecreatefromgif($img_content); break;
		endswitch;
		
		$tmp = imagecreatetruecolor($ori_w, $ori_h);
		
		// preserve transparency
		if($img_type == "gif" or $img_type == "png"){
			imagecolortransparent($tmp, imagecolorallocatealpha($tmp, 0, 0, 0, 127));
			imagealphablending($tmp, false);
			imagesavealpha($tmp, true);
		}
		
		imagecopyresampled($tmp, $src, 0, 0, 0, 0, $ori_w, $ori_h, $ori_w, $ori_h);
		
		switch($img_type):
			case "jpg": imagejpeg($tmp, $folder.$file_up, 100); break;
			case "jpeg": imagejpeg($tmp, $folder.$file_up, 100); break;
			case "png": imagepng($tmp, $folder.$file_up); break;
			case "gif": imagegif($tmp, $folder.$file_up); break;
		endswitch;
		
		### destroy image temp ###
		imagedestroy($tmp);
		imagedestroy($src);
		
		$file_up = urlencode($file_up);
		
		return $file_up;
	}
	
	public function getFileExtension($filename){
	  $ext = pathinfo($filename, PATHINFO_EXTENSION);
	  return $ext;
	}
	
	public function get_width($img_content)
	{
		$ori_size = getimagesize($img_content);
		$ori_w = $ori_size[0];
		
		return $ori_w;
	}
	
	public function get_height($img_content)
	{
		$ori_size = getimagesize($img_content);
		$ori_h = $ori_size[1];
		
		return $ori_h;
	}
	
	public function imgCropUpload($folder = NULL,$filename = NULL,$x = NULL,$y = NULL,$w = NULL,$h = NULL,$dest_image_folder = NULL,$img_resize_width = NULL,$img_resize_height = NULL,$crop_img = FALSE,$resize_img = FALSE, $rename = TRUE,$file_rename = NULL, $clean_img = TRUE)
	{
		
		$imagePath = NULL;
		
		if($folder != NULL && $filename != NULL):
			$img_tmp = $folder.$filename;
		else:
			$img_tmp = $filename;
		endif;
		
		if($rename == TRUE):
			$img_new_name = date('YmdHis').rand(1000,9999);
		else:
			$img_name = explode('.', $file_rename);
			$img_new_name = $img_name[0];
		endif;
		// return print_r($filename);
		
		/*     Crop Image       */
		if(($x != NULL && $y != NULL) || ($x != 0 && $y != 0)):
			
			$ext = strtolower(end(explode('.', $filename)));

			$img_type = strtolower($ext);
			
			switch($img_type):
				case "jpg": $src = imagecreatefromjpeg($img_tmp); break;
				case "jpeg": $src = imagecreatefromjpeg($img_tmp); break;
				case "png": $src = imagecreatefrompng($img_tmp); break;
				case "gif": $src = imagecreatefromgif($img_tmp); break;
			endswitch;
			
			$tmp = imagecreatetruecolor($img_resize_width, $img_resize_height);
			
			// preserve transparency
			if($img_type == "gif" or $img_type == "png"){
				imagecolortransparent($tmp, imagecolorallocatealpha($tmp, 0, 0, 0, 127));
				imagealphablending($tmp, false);
				imagesavealpha($tmp, true);
			}
			
			imagecopyresampled($tmp, $src, 0, 0, $x, $y, $img_resize_width, $img_resize_height, $w, $h);
			
			switch($img_type):
				case "jpg": imagejpeg($tmp, $dest_image_folder.$img_new_name.".".$img_type, 100); break;
				case "jpeg": imagejpeg($tmp, $dest_image_folder.$img_new_name.".".$img_type, 100); break;
				case "png": imagepng($tmp, $dest_image_folder.$img_new_name.".".$img_type); break;
				case "gif": imagegif($tmp, $dest_image_folder.$img_new_name.".".$img_type); break;
			endswitch;
			
			if($clean_img == TRUE && file_exists($img_tmp)) unlink($img_tmp);
			imagedestroy($tmp);
			imagedestroy($src);			
			
			$imagePath = $img_new_name.".".$img_type;
		else:
			
			$image = new upload($img_tmp);
					
			if($image->uploaded):
				
				### crop and resize large image ###
				$image->file_overwrite = TRUE; 
				$image->file_auto_rename	= TRUE;
				$image->file_new_name_body	= $img_new_name;
				$image->jpeg_quality = 100;
				
				if($resize_img == TRUE):
				
					if($crop_img == TRUE):
					$image->image_ratio_crop 	= TRUE;
					endif;
					if($img_resize_width != NULL && $img_resize_height != NULL):
						$image->image_resize		= TRUE;
						$image->image_ratio		 	= TRUE;
						$image->image_x				= $img_resize_width;
						$image->image_y				= $img_resize_height;
					endif;
					if($img_resize_width != NULL && $img_resize_height == NULL):
						$image->image_resize		= TRUE;
						$image->image_ratio		 	= TRUE;
						$image->image_x				= $img_resize_width;
						$image->image_ratio_y		 = TRUE;
					endif;
					if($img_resize_height != NULL && $img_resize_width == NULL):
						$image->image_resize		= TRUE;
						$image->image_ratio		 	= TRUE;
						$image->image_y				= $img_resize_height;
						$image->image_ratio_x		 = TRUE;
					endif;
					
				endif;
				
				$image->Process($dest_image_folder);
				if($image->processed):
					$imagePath =  $image->file_dst_name ;
					if($clean_img == TRUE):
						$image->clean();
					endif;
				else :
					return 'error : ' . $image->error;
				endif;
			else :
				return 'error : No Image';
			endif;
		endif;
		
		return $imagePath;
	}
	
	function fileUpload($file_tmp = NULL,$dest_file_folder = NULL, $rename = TRUE, $file_tmp_name = NULL, $clean_file = TRUE)
	{
		$fileName = NULL;
		
		if($rename == TRUE):
			// check thai language
			$temp_name = current(explode(".", $file_tmp['name']));
			$pattern = '/^([ก-๙])*$/';
			if(preg_match($pattern, $temp_name)):
			   $file_new_name = date('YmdHis').rand(1000,9999);
			else:
			   $file_new_name = $temp_name.'_'.(date('YmdHis').rand(1000,9999));
			endif;
		else:
			$file_new_name = $file_tmp_name;
		endif;
		
		$file = new upload($file_tmp);
					
		if($file->uploaded):
			if($rename == TRUE):
				### crop and resize large image ###
				$file->file_overwrite = TRUE; 
				$file->file_auto_rename	= TRUE;
				$file->file_new_name_body	= $file_new_name;
			endif;
			$file->Process($dest_file_folder);
			if($file->processed):
				$fileName =  $file->file_dst_name ;
				if($clean_file == TRUE):
					$file->clean();
				endif;
			else :
				return 'error : ' . $file->error;
			endif;
		else :
			return 'error : No File';
		endif;
		
		return $fileName;
		
	}
	
}

?>