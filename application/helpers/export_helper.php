<?php

function roomExportJson($query, $output){

    $fileJsonName = 'room.json';    
    // echo "<pre/>";
    // print_r($query->result());
    foreach ($query->result() as $key => $room) {
        // print_r($room);
        // exit();
        
        // echo $room->room_no;
        $roomArr[$room->room_code]['no'] = $room->room_no;
        $roomArr[$room->room_code]['floor'] = $room->floorname;
        $roomArr[$room->room_code]['unitno'] = $room->unit_no;
        $roomArr[$room->room_code]['type'] = $room->room_type;
        $roomArr[$room->room_code]['size'] = (float)$room->size;
        $roomArr[$room->room_code]['view'] = $room->compass;
        $roomArr[$room->room_code]['balcony'] = $room->balcony;
        $roomArr[$room->room_code]['price'] = array('total' => (int)$room->price);
        $roomArr[$room->room_code]['status'] = $room->abbreviation;
    }

    $roomAll = array(
        'rooms' => $roomArr
    );
    // echo json_encode($roomAll, JSON_UNESCAPED_UNICODE);
    // exit();
    switch ($output) {

        case 'file' :
            header("Content-Type: application/json; charset=utf-8");
            header('Content-Disposition: attachment; filename='.$fileJsonName);
            echo json_encode($roomAll, JSON_UNESCAPED_UNICODE);
            break;

        case 'folder' :
            $pathFile = 'Content/xml/'.$fileJsonName;
            write_file($pathFile, json_encode($roomAll, JSON_UNESCAPED_UNICODE));
            break;
            

        case 'direct' :
            return response(json_encode($roomAll, JSON_UNESCAPED_UNICODE))->header('Content-Type', 'application/json');
            break;

        default :
            $pathFile = 'Content/xml/'.$fileJsonName;
            write_file($pathFile, json_encode($roomAll, JSON_UNESCAPED_UNICODE));
            break;
    }
}

function roomExportXml($query, $output){

    $response = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
                ."\n".'<rooms xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
    foreach ($query->result() as $room):
        $response .= _generateXMLRoom($room);
    endforeach;
    $response .= '</rooms>';

	$filename = 'room';

    switch ($output) {

            case 'file' :
                header("Content-type: text/xml; charset=utf-8");
			    header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
			    echo $this->xmlpp($response);
			    exit();
                break;

            case 'folder' :

                //$this->load->helper('file');
                write_file('Content/xml/' . $filename . '.xml', $response);
                break;

            case 'direct' :
                header('Content-type: text/xml');
                echo $response;
                break;

            default :
                header('Content-type: text/xml');
                echo $response;
                break;
        }

}

function roomStatusExportXml($query, $output){

	 $response = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
                ."\n".'<Rooms xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
    foreach ($query->result() as $room):
        $response .= _generateXMLRoomStatus($room);
    endforeach;
    $response .= '</Rooms>';

	$filename = 'masterplan_content';

    switch ($output) {

            case 'file' :
                header("Content-type: text/xml; charset=utf-8");
			    header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
			    echo $this->xmlpp($response);
			    exit();
                break;

            case 'folder' :

                //$this->load->helper('file');
                write_file('Content/xml/' . $filename . '.xml', $response);
                break;

            case 'direct' :
                header('Content-type: text/xml');
                echo $response;
                break;

            default :
                header('Content-type: text/xml');
                echo $response;
                break;
        }
}

function roomTypeExportXml($query, $output){

	$response = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
                ."\n".'<room_type_xml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
    foreach ($query->result() as $room):
        $response .= _generateXMLRoomType($room);
    endforeach;
    $response .= '</room_type_xml>';

	$filename = 'room_type';

    switch ($output) {

            case 'file' :
                header("Content-type: text/xml; charset=utf-8");
			    header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
			    echo $this->xmlpp($response);
			    exit();
                break;

            case 'folder' :

                //$this->load->helper('file');
                write_file('Content/xml/' . $filename . '.xml', $response);
                break;

            case 'direct' :
                header('Content-type: text/xml');
                echo $response;
                break;

            default :
                header('Content-type: text/xml');
                echo $response;
                break;
        }

}

function roomTypeMainExportXml($query, $output){

    $response = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
                ."\n".'<room_main_xml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
    foreach ($query->result() as $room):
        $response .= _generateXMLRoomTypeMain($room);
    endforeach;
    $response .= '</room_main_xml>';

    $filename = 'room_typeMain';

    switch ($output) {

            case 'file' :
                header("Content-type: text/xml; charset=utf-8");
                header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
                echo $this->xmlpp($response);
                exit();
                break;

            case 'folder' :

                //$this->load->helper('file');
                write_file('Content/xml/' . $filename . '.xml', $response);
                break;

            case 'direct' :
                header('Content-type: text/xml');
                echo $response;
                break;

            default :
                header('Content-type: text/xml');
                echo $response;
                break;
        }

}

function galleryExportXml($query, $output){
    $response = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
                ."\n".'<gallery_xml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
    foreach ($query->result() as $gallery):
        $response .= _generateXMLGallery($gallery);
    endforeach;
    $response .= '</gallery_xml>';

    $filename = 'gallery';

    switch ($output) {

            case 'file' :
                header("Content-type: text/xml; charset=utf-8");
                header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
                echo $this->xmlpp($response);
                exit();
                break;

            case 'folder' :

                //$this->load->helper('file');
                write_file('Content/xml/' . $filename . '.xml', $response);
                break;

            case 'direct' :
                header('Content-type: text/xml');
                echo $response;
                break;

            default :
                header('Content-type: text/xml');
                echo $response;
                break;
        }
}

function _generateXMLRoom($roomModel){
    $price = (isset($roomModel->price))?$roomModel->price:"0";
    $price_sqm = (isset($roomModel->price_sqm))?$roomModel->price_sqm:"-";
    $tfs = (isset($roomModel->tfs))?$roomModel->tfs:"-";
    $mb = (isset($roomModel->mb))?$roomModel->mb:0;
    $discount = (isset($roomModel->discount))?$roomModel->discount:"0";
    $budget_price = (isset($roomModel->budget_price))?$roomModel->budget_price:"0";
    $reservation = (isset($roomModel->reservation))?$roomModel->reservation:"0";
    $contract = (isset($roomModel->contract))?$roomModel->contract:"0";
    return "\n".'<room>'
            ."\n\t".'<roommain_name>'.$roomModel->room_code.'</roommain_name>'
            ."\n\t".'<roomtype>'.$roomModel->room_type.'</roomtype>'
            ."\n\t".'<sizeroom>'.$roomModel->size.'</sizeroom>'
            ."\n\t".'<floor>'.$roomModel->floorname.'</floor>'
            ."\n\t".'<type_of_sale>'.$tfs.'</type_of_sale>'
            ."\n\t".'<mb>'.$mb.'</mb>'
            ."\n\t".'<compass>'.$roomModel->compass.'</compass>'
            ."\n\t".'<room_func>'.$roomModel->detail.'</room_func>'
            ."\n\t".'<price>'.$price.'</price>'
            ."\n\t".'<price_sqm>'.$price_sqm.'</price_sqm>'
            ."\n\t".'<discount>'.$discount.'</discount>'
            ."\n\t".'<budget_price>'.$budget_price.'</budget_price>'
            ."\n\t".'<booking>'.$reservation.'</booking>'
            ."\n\t".'<contract>'.$contract.'</contract>'
            // ."\n\t".'<promotion_price>'.'0'.'</promotion_price>'
            // ."\n\t".'<furniture_price>'.'0'.'</furniture_price>'
            // ."\n\t".'<netunit_price>'.'0'.'</netunit_price>'
            ."\n".'</room>';


}

function _generateXMLRoomStatus($roomModel){
    $str = "\n".'<Room>'
            ."\n\t".'<RoomCode>'.$roomModel->room_code.'</RoomCode>';

            switch ($roomModel->status) {
                case '1' : $str .= "\n\t".'<RoomStatus>A</RoomStatus>'; break;
                case '2' : $str .= "\n\t".'<RoomStatus>B</RoomStatus>'; break;
                case '3' : $str .= "\n\t".'<RoomStatus>S</RoomStatus>'; break;
                case '4' : $str .= "\n\t".'<RoomStatus>F</RoomStatus>'; break;
            }
            $str .= "\n".'</Room>';

    return $str;
}

function _generateXMLRoomType($roomModel){
	return "\n".'<room_type>'
            ."\n\t".'<room_name>'.$roomModel->room_type.'</room_name>'
            // ."\n\t".'<room_inProgram>'.$roomModel->room_function.'</room_inProgram>'
            // ."\n\t".'<size>'.$roomModel->size.'</size>'
            // ."\n\t".'<unit_plan>'.$roomModel->image.'</unit_plan>'
            // ."\n\t".'<room_mainID>'.$roomModel->typemain_id.'</room_mainID>'
            ."\n".'</room_type>';
}

function _generateXMLRoomTypeMain($roomModel){
    return "\n".'<room_main>'
            ."\n\t".'<roommain_id>'.$roomModel->id.'</roommain_id>'
            ."\n\t".'<roommain_name>'.$roomModel->name.'</roommain_name>'
            ."\n\t".'<amountSize>'.$roomModel->size.'</amountSize>'
            ."\n\t".'<unitplan>'.$roomModel->images.'</unitplan>'
            ."\n\t".'<bedroom>'.$roomModel->bedroom.'</bedroom>'
            ."\n\t".'<bathroom>'.$roomModel->bathroom.'</bathroom>'
            ."\n".'</room_main>';
}

function _generateXMLGallery($gallery){
    return "\n".'<gallery_main>'
            ."\n\t".'<pic_id>'.$gallery->id.'</pic_id>'
            ."\n\t".'<pathImage_thumb>'.$gallery->thumbnail.'</pathImage_thumb>'
            ."\n\t".'<pathImage>'.$gallery->url.'</pathImage>'
            ."\n\t".'<textImage>'.$gallery->name.'</textImage>'
            ."\n".'</gallery_main>';
}

?>
