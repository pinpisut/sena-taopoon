	<h1 style="text-align:center;">Rich Park @Triple station</h1>
	<table>
		<tr>
			<td style="width:40%">
				<h3>Floor Plan</h3>
				<img src="<?php echo base_url().$query[0]->floorimg; ?>" alt="" width:"200" height="150">
				<h3>Room Plan</h3>
				<img src="<?php echo base_url().$query[0]->roomimg; ?>" alt="" width:"200" height="150">
				<h3 style="font-size:12px;">Sale Gallery : 02-886-1817 #514</h3>
			</td>
			<td style="width:60%;" >
				<h6 style="text-align:right;">วันที่ : <?php echo date("Y / m / d"); ?></h6>
				<div style="right: 0;">
					<table border="1" style="font-size:10px;width:100%;text-align:center;">
						<tr style="background-color:#D3D3D3;">
							<th>Floor<br/>No.</th>
							<th>Unit No.</th>
							<th>Area<br/>(Sq.m.)</th>
							<th>Room Type</th>
							<th>Unit Price</th>
							<th>Furniture</th>
							<th>Promotion</th>
							<th>Net Unit Price</th>						
						</tr>
						<tr>
							<td><?php echo $query[0]->floor; ?></td>
							<td><?php echo $query[0]->room_code; ?></td>
							<td><?php echo $query[0]->size; ?></td>
							<td><?php echo $query[0]->room_type; ?></td>
							<td><?php echo number_format($query[0]->price); ?></td>
							<td><?php echo "0"; ?></td>
							<td>
								<?php 
									$discount = ($query[1]['discount'])?$query[1]['discount']:0;
									echo number_format($discount); 
								?>
							</td>
							<td><?php echo number_format($query[0]->price); ?></td>
						</tr>
					</table>
				</div>
				<table style="font-size:10px;width:100%;">
					<tr>
						<td>ราคาขาย</td>
						<td style="text-align:right;"><?php echo number_format($query[0]->price); ?> บาท</td>
					</tr>
					<tr>
						<td>เงินจอง</td>
						<td style="text-align:right;"><?php echo number_format($query[0]->reservation); ?> บาท</td>
					</tr>
					<tr>
						<td>เงินทำสัญญา</td>
						<td style="text-align:right;"><?php echo number_format($query[0]->contract); ?> บาท</td>
					</tr>
					<tr>
						<td>เงินดาวน์ประมาณ 10% ของราคาขาย</td>
						<td></td>
					</tr>
					<tr>
						<td>ผ่อนดาวน์จำนวน 24 งวด งวดละ</td>
						<td style="text-align:right;"><?php echo number_format($query[0]->installment); ?> บาท</td>
					</tr>
					<tr>
						<td>รวมชำระดาวน์</td>
						<td style="text-align:right;"><?php echo number_format($query[0]->installment*24); ?> บาท</td>
					</tr>
				</table>
				<!-- <h6 style="text-align:center;">ผ่อนดาวน์จำนวน 30 งวด</h6> -->
				
					<!-- <table border="1" style="font-size:10px;width:100%;text-align:center;">
						<tr style="background-color:#D3D3D3;">
							<th>งวด 1-3</th>
							<th>งวด 5-7</th>
							<th>งวด 9-11</th>
							<th>งวด 13-15</th>
							<th>งวด 17-19</th>
							<th>งวด 21-23</th>
							<th>งวด 25-27</th>
							<th>งวด 29-30</th>					
						</tr>
						<tr>
							<td><?php //echo number_format($query[0]->installment); ?></td>
							<td><?php //echo number_format($query[0]->installment); ?></td>
							<td><?php //echo number_format($query[0]->installment); ?></td>
							<td><?php //echo number_format($query[0]->installment); ?></td>
							<td><?php //echo number_format($query[0]->installment); ?></td>
							<td><?php //echo number_format($query[0]->installment); ?></td>
							<td><?php //echo number_format($query[0]->installment); ?></td>
							<td><?php //echo number_format($query[0]->installment); ?></td>
						</tr>						
					</table>
					<table border="1" style="font-size:10px;width:100%;text-align:center;">
						<tr style="background-color:#D3D3D3;">
							<th>งวด 4</th>
							<th>งวด 8</th>
							<th>งวด 12</th>
							<th>งวด 16</th>
							<th>งวด 20</th>
							<th>งวด 24</th>
							<th>งวด 28</th>				
						</tr>
						<tr>
							<td><?php //echo number_format($query[0]->balloon); ?></td>
							<td><?php //echo number_format($query[0]->balloon); ?></td>
							<td><?php //echo number_format($query[0]->balloon); ?></td>
							<td><?php //echo number_format($query[0]->balloon); ?></td>
							<td><?php //echo number_format($query[0]->balloon); ?></td>
							<td><?php //echo number_format($query[0]->balloon); ?></td>
							<td><?php //echo number_format($query[0]->balloon); ?></td>
						</tr>
					</table> -->
				<table style="font-size:10px;width:100%;">
					<tr>
						<td>รวมชำระดาวน์ทั้งหมด + เงินจอง + เงินทำสัญญา</td>
						<td style="text-align:right;"><?php echo number_format($query[0]->amount); ?> บาท</td>
					</tr>
				</table>
				<table style="font-size:10px;width:100%;">
					<tr>
						<td>โอนกรรมสิทธิ์</td>
						<td style="text-align:right;"><?php echo number_format($query[0]->ownership); ?> บาท</td>
					</tr>
					<!-- <tr>
						<td>ประมาณการ การผ่อนชำระธนาคาร</td>
						<td style="text-align:right;">30 ปี</td>
					</tr>
					<tr>
						<td>อัตราดอกเบี้ยธนาคาร</td>
						<td style="text-align:right;">7% ต่อปี</td>
					</tr> -->
					<!-- <tr>
						<td>ผ่อนชำระธนาคารเดือนละ</td>
						<td style="text-align:right;">?????????</td>
					</tr> -->
				</table>
				<h6 style="font-size:3px;">&nbsp;</h6>
				<table border="1" style="font-size:10px;width:100%;">
					<tr style="background-color:#D3D3D3;">
						<td colspan="3">รายการค่าใช้จ่ายที่ลูกค้าต้องชำระในวันโอนกรรมสิทธิ์ ดังนี้</td>
					</tr>
					<tr>
						<td style="width:20%;">ค่าใช้จ่ายส่วนกลาง</td>
						<td style="width:60%;">จัดเก็บล่วงหน้าเป็นรายปี เว้นแต่ครั้งแรก ชำระในวันโอนกรรมสิทธิ์</td>
						<td style="width:20%;">35 บาท/ ตร.ม./ เดือน</td>
					</tr>
					<tr>
						<td>เงินกองทุน</td>
						<td>จัดเก็บครั้งเดียว ชำระในวันโอนกรรมสิทธิ์</td>
						<td>500 บาท/ตร.ม.</td>
					</tr>
					<tr>
						<td colspan="3">ค่าธรรมเนียม/ค่าใช้จ่ายในการติดตั้ง และเงินประกัน มิเตอร์ไฟฟ้า จัดเก็บครั้งเดียว ชำระให้กับการไฟฟ้า<br/>แจ้งค่าใช้จ่ายให้ทราบเมื่อการก่อสร้างอาคารแล้วเสร็จ</td>
					</tr>
					<tr>
						<td colspan="3">ค่าจดทะเบียนโอนกรรมสิทธิ์ 1% ของราคาประเมิน หรือตามอัตราที่กฎหมายกำหนด</td>
					</tr>
					<tr>
						<td colspan="3">ค่าจดทะเบียนจำนอง 1% ของวงเงินสินเชื่อ หรือตามที่กฎหมายกำหนด (เฉพาะกรณีกูเงินกับสถาบันการเงิน)</td>
					</tr>
				</table>
				<h6>เงื่อนไขเพิ่มเติม:</h6>
				<table style="font-size:10px;width:100%;">
					<tr>
						<td colspan="2">ชื่อ-สกุลลูกค้า: <?php echo $query[1]['firstname']."  ".$query[1]['lastname']; ?></td>
					</tr>
					<tr>
						<td>โทรศัพท์ลูกค้า: <?php echo $query[1]['tel']; ?></td>
						<td >Email: <?php echo $query[1]['email']; ?></td>
					</tr>
				</table>
				<h6>หมายเหตุ:</h3>
				<ul style="font-size:10px;">
					<li>ตำแหน่งและขนาดห้องชุดที่ระบุในเอกสารนี้ อาจปรับเปลี่ยนได้ตามความเหมาะสม โดยไม่กระทบต่อสาระสำคัญในการใช้งานและพักอาศัย ภาพ และบรรยากาศ</li>
					<li>ราคาและเงื่อนไขตามเอกสารนี้ อาจมีการเปลี่ยนแปลงได้โดยมิต้องแจ้งให้ทราบล่วงหน้า</li>
				</ul>
			</td>
		</tr>
	</table>