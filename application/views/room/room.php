<!DOCTYPE html>
<html>
    <head>
        <title>
            Room
        </title>
        <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/font-awesome.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/se7en-font.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/datatables.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/bootstrap-editable.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/style.css" media="all" rel="stylesheet" type="text/css" />

        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" charset="utf-8">
    </head>
    <body class="page-header-fixed bg-3">
    	<div class="modal-shiftfix">
    		<!-- Navigation -->
		        <div class="navbar navbar-fixed-top scroll-hide">
                <?php  $this->load->view('include/top_bar_menu'); ?>
		            <?php  $this->load->view('include/main_menu'); ?>
		        </div>
	        <!-- End Navigation -->

            <div class="container-fluid main-content">
                <div class="page-title">
                  <h1 style="display: inline-block;">
                    Room Information
                  </h1>
                  <!--<div class="btn-group" style="float: right;">
                    <a href="php_xml/exportexcel">
                      <button class="btn btn-warning">Download Excel</button>
                    </a>
                  </div>-->
                </div>


                <!-- Start Confirm Alert -->
                <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                Delete Dialog
                            </div>
                            <div class="modal-body">
                                Are you sure?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <a href="" class="btn btn-danger danger delete-row" id="remove">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Confirm Alert -->


                <!-- DataTables Example -->
                <div class="row">
                  <div class="col-lg-12">
                    <div class="widget-container fluid-height clearfix" style="overflow-y: scroll;">
                      <div class="heading">
                        <i class="fa fa-table"></i>DataTable with Sorting
                        <!-- <a class="btn btn-sm btn-primary-outline pull-right" href="#" id="add-row"><i class="fa fa-plus"></i>Add row</a> -->
                      </div>
                      <div class="widget-content padded clearfix">
                        <table class="table table-bordered" id="datatable-editable-room">
                          <thead>
                            <th>Room Code.</th>
                            <th>Floor</th>
                            <th>No.</th>
                            <th>Unit No.</th>                            
                            <th>Room Type</th>
                            <th>Area (sq.m.)</th>
                            <th>Price</th>
                            <th>Compass</th>
                            <th>Status</th>

                            <th width="60"></th>
                            <!--<th width="75"></th>-->
                          </thead>
                          <tbody>
                              <?php foreach ($results as $result) { ?>
                                <?php
                                  if($result->status == 'Sold'){ $classState = 'label-sold'; }
                                  elseif($result->status == 'Available') { $classState = 'label-available'; }
                                  else{ $classState = 'label-block'; }
                                ?>
                                <tr data-roomID="<?php echo $result->id; ?>" data-qid="<?php echo $result->room_code; ?>" class="<?php //echo $classState; ?>">
                                  <td>
                                        <?php echo $result->room_code ?>
                                  </td>
                                  <td>
                                        <?php echo $result->floorname ?>
                                  </td>
                                  <td>
                                        <?php echo $result->room_no ?>
                                  </td>
                                  <td>
                                        <?php echo $result->unit_no ?>
                                  </td>
                                  <td>
                                        <?php echo $result->room_type ?>
                                  </td>
                                  <td>
                                        <?php echo $result->size ?>
                                  </td>
                                  <td>
                                        <?php echo $result->price ?>
                                  </td>
                                  <td>
                                        <?php echo $result->compass ?>
                                  </td>
                                  <td>
                                        <?php echo $result->status ?>
                                  </td>
                                  <td>
                                    <a class="edit-row" href="">Edit</a>
                                  </td>
                                  <!--<td>
                                    <a class="delete-row" href="">Delete</a>
                                  </td>-->
                                </tr>
                              <?php }?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end DataTables Example -->
              </div>
            </div>

    	</div>

      <script>
            var list_tower = <?php echo $list_tower; ?>;
            var list_roomType = <?php echo $list_roomType; ?>;
            var list_status = <?php echo $list_status; ?>;
            var list_floor = <?php echo $list_floor; ?>;
            var list_floor_first = <?php echo $list_floor_first; ?>;
            var site_url = '<?php echo site_url()?>';
      </script>

    	<script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
      <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/format_date.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/selectivizr-min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/jquery.dataTables.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/table-room.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/jquery.validate.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/bootstrap-editable.min.js" type="text/javascript"></script>
    </body>
</html>
