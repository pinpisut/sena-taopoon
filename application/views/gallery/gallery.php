<!DOCTYPE html>
<html>
    <head>
        <title>
            tower
        </title>
        <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/font-awesome.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/se7en-font.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/datatables.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/bootstrap-editable.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/jquery.fileupload-ui.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>vendors/bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" charset="utf-8">
        <style>
            .kv-fileinput-upload{
               display: none !important;
            }

            /*.button-submit-modal{
                left: 0;
                right: 0;
                margin: auto !important;
            }*/

            .btn-top{
                margin-top: 20px;
            }

            .row.button-submit-modal {
              text-align: center;
            }
        </style>
    </head>
    <body class="page-header-fixed bg-3">
    	<div class="modal-shiftfix">
    		<!-- Navigation -->
		        <div class="navbar navbar-fixed-top scroll-hide"> 
                <?php  $this->load->view('include/top_bar_menu'); ?>          
		            <?php  $this->load->view('include/main_menu'); ?>
		        </div>
	        <!-- End Navigation -->

            <div class="container-fluid main-content">
                <div class="page-title">
                  <h1>
                    Gallery
                  </h1>
                </div>
                <!-- Start Confirm Alert -->
                <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                Delete Dialog
                            </div>
                            <div class="modal-body">
                                Are you sure?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <a href="" class="btn btn-danger danger delete-row" id="remove">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Confirm Alert -->

                <!-- DataTables Example -->
                <div class="row">
                  <div class="col-lg-12">
                    <div class="widget-container fluid-height clearfix" style="overflow-y: scroll;">
                      <div class="heading">
                        <i class="fa fa-table"></i>DataTable with Sorting<a class="btn btn-sm btn-primary-outline pull-right detail-row" href="#"><i class="fa fa-plus"></i>Add row</a>
                      </div><!-- id="add-row"   detail-row -->
                      <div class="widget-content padded clearfix">
                        <table class="table table-bordered table-striped" id="datatable-editable-gallery">
                          <thead>
                            <th>
                              Type
                            </th>
                            <th>
                              Image ( Size 1920 x 858 px )
                            </th>
                            <th>
                              Thumbnail ( Size 258 x 200 px )
                            </th>
                            <th>
                              Name
                            </th>
                            <!-- <th>
                              Instance Name
                            </th> -->
                            <th width="100"></th>
                            <th width="100"></th>
                          </thead>
                          <tbody>   
                            <?php foreach ($results as $result) { ?>
                              <tr data-id="<?php echo $result->id ?>">
                                <td>
                                  <?php echo $result->type ?>
                                </td>
                                <td>
                                  <img src="<?php echo base_url() ?><?php echo ($result->url)?$result->url:'Content/images/no-image.png' ?>" width="200px" height="150px">
                                  <button style="display:none;" id="img<?php echo $result->id?>" class="image-gallery" data-width="1750" data-height="845">img</button>
                                
                                </td>
                                <td>
                                  <img src="<?php echo base_url() ?><?php echo ($result->thumbnail)?$result->thumbnail:'Content/images/no-image.png' ?>" width="200px" height="150px">
                                  <button style="display:none;" id="temp<?php echo $result->id?>" class="thumbnail-gallery" data-width="446" data-height="268">thumbnail</button>
                                  
                                </td>
                                <td>
                                  <?php echo $result->name ?>
                                </td>
                                <!-- <td>
                                  <?php //echo $result->instance_name ?>
                                </td> -->
                                <td>
                                  <a class="edit-row" href="">Edit</a>
                                </td>
                                <td>
                                  <a class="delete-row" href="">Delete</a>
                                </td>
                              </tr>
                            <?php }?>
                          </tbody>
                        </table>
                      </div>

                  <!-- ************ Modal Detail Start *************** -->
                      <div class="modal fade" id="myModal">
                        <div class="modal-dialog">
                          <div class="modal-content" style="width: 700px;">
                            <div class="modal-header">
                              <!-- <button aria-hidden="true" class="close" data-dismiss="modal" type="button">OK</button> -->
                              <h4 class="modal-title">
                                  Add Gallery
                              </h4>
                              
                            </div>
                          <?php echo form_open_multipart('gallery_controller/insert'); ?>
                            <!-- <form action="gallery_controller/createGallery" method="post" class="form-horizontal" enctype="multipart/form-data"> -->
                            <!--{{ Form::open(array('url' => 'update-stat', 'id' => 'update-stat')) }}-->
                                <div class="modal-body" style="height: 700px;">                              
                                    <h3 class="id"></h3>

                                    <div class="row">
                                      <div class="form-group">                                  
                                            <label class="control-label col-md-3">Type</label>
                                            <div class="col-md-7">
                                              <select class="form-control" name="galleryType">
                                                <option value="Gallary">Gallery</option>
                                                <!-- <option value="Funiture">Furniture</option> -->
                                              </select>
                                            </div>
                                      </div>
                                    </div>

                                    <div class="row">
                                        <label class="control-label col-md-3">Image (1760 x 850)</label>
                                        <div class="col-md-7">
                                          <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                                              <img src="<?php echo base_url() ?>images/no-image.png">
                                            </div>
                                            <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px"></div>
                                            <div>
                                              <span class="btn btn-default btn-file">
                                                <span class="fileupload-new">Select image</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input id="file-0" type="file" name="userfile" multiple="multiple">
                                              </span>
                                                <a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                                            </div>
                                          </div>
                                           <!-- <input id="file-0" type="file" name="userfile" multiple="multiple"  /> -->
                                        </div>
                                    </div>

                                    <div class="row">
                                        <label class="control-label col-md-3">Thumbnail</label>
                                        <div class="col-md-7">
                                          <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                                              <img src="<?php echo base_url() ?>images/no-image.png">
                                            </div>
                                            <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px"></div>
                                            <div>
                                              <span class="btn btn-default btn-file">
                                                <span class="fileupload-new">Select image</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input id="thumb" type="file" name="thumb" multiple="multiple">
                                              </span>
                                                <a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                                            </div>
                                          </div>
                                           <!-- <input id="thumb" type="file" name="thumb" multiple="multiple"  /> -->
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                          <label class="control-label col-md-3">Name</label>
                                          <div class="col-md-7">
                                            <input class="form-control" placeholder="Text" type="text" id="caption" name="caption">
                                          </div>
                                        </div>
                                    </div>

                                   <!--  <div class="row">
                                        <div class="form-group">
                                          <label class="control-label col-md-3">Instance Name</label>
                                          <div class="col-md-7">
                                            <input class="form-control" placeholder="Text" type="text" id="ins_name" name="ins_name">
                                          </div>
                                        </div>
                                    </div> -->

                                    <div class="row button-submit-modal">
                                        <button type="submit" class="btn btn-primary btn-top">Submit</button>
                                        <button type="reset" class="btn btn-default btn-top">Reset</button>
                                    </div>                                
                                    <!-- <h3 class="gallery_id"></h3> -->
                                </div>
                            <!-- </form> -->
                            <?php echo "</form>"?>
                            <!--{{ Form::close() }}-->
                          </div>
                        </div>
                      </div>
                  <!-- ************ Modal Detail End *************** -->


                    </div>
                  </div>
                </div>
                <!-- end DataTables Example -->
              </div>
            </div>

    	</div>
		  <script> 
          var url = '<?php echo site_url('gallery_controller/createGallery')?>';
          var site_url = '<?php echo site_url()?>';
      </script>
    	<script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
      <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/selectivizr-min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/jquery.dataTables.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url();?>javascripts/ajaxupload.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/table-gallery.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/jquery.validate.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/bootstrap-fileupload.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>vendors/bootstrap-fileinput/js/fileinput.min.js" type="text/javascript"></script>
      <script>
        $('.detail-row').click(function(e){
            e.preventDefault();   
            var galleryid = $(this).data('galleryid');
            $(".id").text($(this).data('galleryid'));

            // $(".gallery_id").text($(this).data('galleryid'));
            // $(".id-na").text("รายชื่อห้อง");
            $('#myModal').modal('show');             
        });

        // $("#file-0").fileinput({
        //     'allowedFileExtensions' : ['jpg', 'png','gif'],
        // });
        
        // $("#thumb").fileinput({
        //     'allowedFileExtensions' : ['jpg', 'png','gif'],
        // });

    </script>
    </body>
</html>