<!DOCTYPE html>
<html>
    <head>
        <title>
            Room Type
        </title>
        <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/font-awesome.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/se7en-font.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/datatables.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/bootstrap-editable.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/jquery.fileupload-ui.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>vendors/bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" charset="utf-8">
        <style>
            .row.button-submit-modal {
              text-align: center;
            }
        </style>
    </head>
    <body class="page-header-fixed bg-3">
    	<div class="modal-shiftfix">
    		<!-- Navigation -->
		        <div class="navbar navbar-fixed-top scroll-hide">   
                <?php  $this->load->view('include/top_bar_menu'); ?>        
		            <?php  $this->load->view('include/main_menu'); ?>
		        </div>
	        <!-- End Navigation -->

            <div class="container-fluid main-content">
                <div class="page-title">
                  <h1>
                    Room Type Information
                  </h1>
                </div>

                <!-- Start Confirm Alert -->
                <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                Delete Dialog
                            </div>
                            <div class="modal-body">
                                Are you sure?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <a href="" class="btn btn-danger danger delete-row" id="remove">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Confirm Alert -->

                <!-- DataTables Example -->
                <div class="row">
                  <div class="col-lg-12">
                    <div class="widget-container fluid-height clearfix" style="overflow-y: scroll;">
                      <div class="heading">
                        <i class="fa fa-table"></i>DataTable with Sorting
                        <!-- <a class="btn btn-sm btn-primary-outline pull-right detail-row" href="#"><i class="fa fa-plus"></i>Add row</a> -->
                      </div>
                      <div class="widget-content padded clearfix">
                        <table class="table table-bordered table-striped" id="datatable-editable-roomtype">
                          <thead>
                            <th>Room Type</th>
                        <!--     <th>Room InProgram</th>
                            <th>size ( sq.m. )</th>
                            <th>Room Main</th>  
                            <th>Images ( Size 580 x 690 px )</th>  -->
                                                 
                           <!--  <th width="60"></th>
                            <th width="75"></th> -->
                          </thead>
                          <tbody>   
                              <?php foreach ($results as $result) { ?>
                                <tr data-roomID="<?php echo $result->room_id ?>">
                                  <td>
                                        <?php echo $result->room_type ?>
                                  </td>
                                <!--   <td>
                                        <?php echo $result->room_function ?>
                                  </td>
                                  <td>
                                        <?php echo $result->size ?>
                                  </td>
                                  <td>
                                        <?php echo $result->name ?>
                                  </td>
                                  <td>
                                        <img src="<?php echo base_url() ?>/<?php echo ($result->image)?$result->image:'Content/images/no-image.png' ?>" width="200px" height="150px">
                                        <button style="display:none;" id="img<?php echo $result->room_id?>" class="url_room" data-width="1750" data-height="845">room</button>
                                  </td> -->
                                  

                                 <!--  <td>
                                    <a class="edit-row" href="">Edit</a>
                                  </td>
                                  <td>
                                    <a class="delete-row" href="">Delete</a>
                                  </td> -->
                                </tr>
                              <?php }?> 
                          </tbody>
                        </table>
                      </div>

                      <!-- ************ Modal Detail Start *************** -->
                      <div class="modal fade" id="myModal">
                        <div class="modal-dialog">
                          <div class="modal-content" style="width: 700px;">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">
                                  Add Room Type
                              </h4>
                              
                            </div>
                          <?php echo form_open_multipart('roomtype_controller/insert'); ?>
                                <div class="modal-body" style="height: 700px;">

                                    <div class="row">
                                        <div class="form-group">
                                          <label class="control-label col-md-3">Room Type</label>
                                          <div class="col-md-7">
                                            <input class="form-control" placeholder="Room Type" type="text" id="roomtype" name="roomtype">
                                          </div>
                                        </div>
                                    </div>

                                    

                                    <div class="row">
                                        <div class="form-group">
                                          <label class="control-label col-md-3">Room InProgram</label>
                                          <div class="col-md-7">
                                            <input class="form-control" placeholder="Room InProgram" type="text" id="inprogram" name="inprogram">
                                          </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                          <label class="control-label col-md-3">Size ( Sq.m. )</label>
                                          <div class="col-md-7">
                                            <input class="form-control" placeholder="Size" type="text" id="size" name="size">
                                          </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                      <div class="form-group">                                  
                                            <label class="control-label col-md-3">Room Type Main</label>
                                            <div class="col-md-7">
                                              <select class="form-control" name="roomfunction">
                                                <option value="1">A</option>
                                                <option value="2">B</option>
                                                <option value="3">C</option>
                                                <option value="4">D</option>
                                              </select>
                                            </div>
                                      </div>
                                    </div>
                                    
                                    <div class="row">
                                        <label class="control-label col-md-3">Images</label>
                                        <div class="col-md-7">
                                          <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                                              <img src="<?php echo base_url() ?>images/no-image.png">
                                            </div>
                                            <div class="fileupload-preview fileupload-exists img-thumbnail" style="width: 200px; max-height: 150px"></div>
                                            <div>
                                              <span class="btn btn-default btn-file">
                                                <span class="fileupload-new">Select image</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input id="file-0" type="file" name="urlroom" multiple="multiple">
                                              </span>
                                                <a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                                            </div>
                                          </div>
                                           <!-- <input id="file-0" type="file" name="urlroom" multiple="multiple"  /> -->
                                        </div>
                                    </div>                                 

                                    <div class="row button-submit-modal">
                                        <button type="submit" class="btn btn-primary btn-top">Submit</button>
                                        <button type="reset" class="btn btn-default btn-top">Reset</button>
                                    </div>
                                </div>
                            <?php echo "</form>"?>
                          </div>
                        </div>
                      </div>
                  <!-- ************ Modal Detail End *************** -->

                    </div>
                  </div>
                </div>
                <!-- end DataTables Example -->
              </div>
            </div>

    	</div>

      <script> 
          var url = '<?php echo site_url('roomtype_controller/updateImg')?>';
          var site_url = '<?php echo site_url()?>';
          var list_func = <?php echo $list_func ?>;
      </script>
		
    	<script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
      <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/selectivizr-min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/jquery.dataTables.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url();?>javascripts/ajaxupload.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/table-roomtype.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/jquery.validate.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/bootstrap-editable.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/bootstrap-fileupload.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>vendors/bootstrap-fileinput/js/fileinput.min.js" type="text/javascript"></script>

      <script>
        $('.detail-row').click(function(e){
            e.preventDefault();   
            var galleryid = $(this).data('galleryid');
            $(".id").text($(this).data('galleryid'));

            // $(".gallery_id").text($(this).data('galleryid'));
            // $(".id-na").text("รายชื่อห้อง");
            $('#myModal').modal('show');             
        });

    </script>
    </body>
</html>