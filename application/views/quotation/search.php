<!DOCTYPE html>
<html>
    <head>
        <title>
            Quotation
        </title>
        <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/font-awesome.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/se7en-font.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/datatables.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/bootstrap-editable.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/jquery.fileupload-ui.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>vendors/bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" charset="utf-8">
        <style>
            .row.button-submit-modal {
              text-align: center;
            }
        </style>
    </head>
    <body class="page-header-fixed bg-3">
    	<div class="modal-shiftfix">
    		<!-- Navigation -->
		        <div class="navbar navbar-fixed-top scroll-hide">   
                <?php  $this->load->view('include/top_bar_menu'); ?>        
		            <?php  $this->load->view('include/main_menu'); ?>
		        </div>
	        <!-- End Navigation -->

            <div class="container-fluid main-content">
                <div class="page-title">
                  <h1>
                    Search Quotation
                  </h1>
                </div>             

                <!-- Start Confirm Alert -->
                <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                Delete Dialog
                            </div>
                            <div class="modal-body">
                                Are you sure?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <a href="" class="btn btn-danger danger delete-row" id="remove">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Confirm Alert -->

                <!-- DataTables Example -->
                <div class="row">
                  <div class="col-lg-12">
                    <div class="widget-container fluid-height clearfix" style="overflow-y: scroll;">
                      <div class="heading">
                        <!-- <i class="fa fa-table"></i>DataTable with Sorting<a class="btn btn-sm btn-primary-outline pull-right detail-row" href="#"><i class="fa fa-plus"></i>Add row</a> -->
                      </div>
                      <div class="widget-content padded clearfix">
                        <table class="table table-bordered table-striped" id="datatable-quotation">
                          <thead>
                            <th>Quotation No.</th>
                            <th>Room</th>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Email</th>
                            <th>Tel</th>
                            <th>Date</th>
                            <th width="75"></th>
                          </thead>
                          <tbody>   
                              <?php foreach ($results as $result) { ?>
                                <tr data-quotation="<?php echo $result->id ?>">
                                  <td><a href="<?php echo base_url()."Content/quotation/".$result->quotation.".pdf"; ?>" target="_blank"><?php echo $result->quotation ?></a></td>
                                  <td><?php echo $result->roomcode ?></td>
                                  <td><?php echo $result->firstname ?></td>
                                  <td ><?php echo $result->lastname ?></td>
                                  <td><?php echo $result->email ?></td>
                                  <td><?php echo $result->tel ?></td>
                                  <td ><?php echo $result->date ?></td>
                                  <td>
                                    <a class="delete-row" href="">Delete</a>
                                  </td>
                                </tr>
                              <?php }?> 
                          </tbody>
                        </table>
                      </div>                  

                    </div>
                  </div>
                </div>
                <!-- end DataTables Example -->
              </div>
            </div>

    	</div>
		
    	<script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
      <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/selectivizr-min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/jquery.dataTables.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url();?>javascripts/ajaxupload.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/table-quotation.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/jquery.validate.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/bootstrap-editable.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/bootstrap-fileupload.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>vendors/bootstrap-fileinput/js/fileinput.min.js" type="text/javascript"></script>

      <script>
        var delete_user = '<?php echo site_url()?>';
      </script>
    </body>
</html>