<!DOCTYPE html>
<html>
    <head>
        <title>
            Quotation
        </title>
        <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/font-awesome.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/se7en-font.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/datatables.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/bootstrap-editable.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/jquery.fileupload-ui.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>vendors/bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/style.css" media="all" rel="stylesheet" type="text/css" />

        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" charset="utf-8">
        <style>
            .row.button-submit-modal {
              text-align: center;
            }
        </style>
    </head>
    <body class="page-header-fixed bg-3">
    <?php
        $uploadedStatus = 0;
        if ( isset($_POST["submit"]) ) {
          if ( isset($_FILES["file"])) {
          //if there was an error uploading the file
            if ($_FILES["file"]["error"] > 0) {
              echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
            } else {
              if (file_exists($_FILES["file"]["name"])) {
                unlink($_FILES["file"]["name"]);
              }
              $storagename = "discussdesk.xlsx";
              move_uploaded_file($_FILES["file"]["tmp_name"], $storagename);
              $uploadedStatus = 1;
            }
          } else {
            echo "No file selected <br />";
          }
        }
    ?>
    	<div class="modal-shiftfix">
    		<!-- Navigation -->
		        <div class="navbar navbar-fixed-top scroll-hide">
                <?php  $this->load->view('include/top_bar_menu'); ?>
		            <?php  $this->load->view('include/main_menu'); ?>
		        </div>
	        <!-- End Navigation -->

            <div class="container-fluid main-content">
                <div class="page-title">
                  <h1>
                    Manage Excel
                  </h1>
                </div>

                <div class="row">
                  <div class="col-lg-12">
                    <div class="widget-container fluid-height clearfix">
                      <div class="heading">
                        <i class="fa fa-cloud-upload"></i>Import Room Data
                      </div>
                      <div class="widget-content padded">
                        <form action="<?php echo base_url()."index.php/excel_controller/do_upload"?>" method="post" enctype="multipart/form-data" class="form-horizontal" id="upload_file">
                          <div class="form-group">
                            <label class="control-label col-md-2">File Excel</label>
                            <div class="col-md-4">
                              <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-group">
                                  <div class="form-control">
                                    <i class="fa fa-file fileupload-exists"></i><span class="fileupload-preview"></span>
                                  </div>
                                  <div class="input-group-btn">
                                    <a class="btn btn-default fileupload-exists" data-dismiss="fileupload" href="#">Remove</a>
                                    <span class="btn btn-default btn-file">
                                      <span class="fileupload-new">Select file</span>
                                      <span class="fileupload-exists">Change</span>
                                      <input type="file" name="file" id="demo1">
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-4">
                              <button type="submit" form="upload_file" value="Submit" class="btn btn-primary import-excel">Import</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-12">
                    <div class="widget-container fluid-height clearfix">
                      <div class="heading">
                        <i class="fa fa-cloud-upload"></i>Export Room Data
                      </div>
                      <div class="widget-content padded">
                        <div class="form-group">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-4">
                              <button class="btn btn-info gen-bottom" id="download-excel">
                                <i class="fa fa-cloud-download"></i>Download File Excel
                              </button>
                            </div>
                          </div>

                      </div>
                    </div>
                  </div>
                </div>

                <!--<div class="row">
                  <div class="col-lg-12">
                    <div class="widget-container fluid-height clearfix">
                      <div class="heading">
                        <i class="fa fa-cloud-upload"></i>Export Customer Data
                      </div>
                      <div class="widget-content padded">
                        <div class="form-group">
                            <label class="control-label col-md-2"></label>
                            <div class="col-md-4">
                              <button class="btn btn-info gen-bottom" id="download-customer">
                                <i class="fa fa-cloud-download"></i>Download File Excel
                              </button>
                            </div>
                          </div>

                      </div>
                    </div>
                  </div>
                </div>-->

            </div>

            <div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Download Excel</h4>
                  </div>
                  <div class="modal-body">
                    <p>Download???</p>
                  </div>
                  <div class="modal-footer">
                    <a href="<?php echo base_url().'knightsbridge_data.xls'; ?>" class="btn btn-primary download">
                      Download
                      <!-- <button type="button" class="btn btn-primary" data-dismiss="modal">Download</button> -->
                    </a>

                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>

            <div class="modal fade" id="customerExcel" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Download Excel</h4>
                  </div>
                  <div class="modal-body">
                    <p>Download???</p>
                  </div>
                  <div class="modal-footer">
                    <a href="<?php echo base_url().'customer_data.xls'; ?>" class="btn btn-primary download-customer">
                      Download
                      <!-- <button type="button" class="btn btn-primary" data-dismiss="modal">Download</button> -->
                    </a>

                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
    	</div>

    	<script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
      <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/selectivizr-min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/jquery.dataTables.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url();?>javascripts/ajaxupload.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/jquery.validate.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/bootstrap-editable.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/bootstrap-fileupload.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>vendors/bootstrap-fileinput/js/fileinput.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url() ?>javascripts/jquery.ajaxfileupload.js" type="text/javascript"></script>
      <script>
          $(".download").on("click", function() {
            $('#myModal').modal('hide');
          });

          $(".download-customer").on("click", function() {
            $('#customerExcel').modal('hide');
          });

          $( "#download-excel" ).on( "click", function() {
            $.ajax({
              url: 'excel_controller/exportexcel'
            }).done(function() {
              $('#myModal').modal('show');
            });
          });

          $( "#download-customer" ).on( "click", function() {
            $.ajax({
              url: 'excel_controller/exportCustomer'
            }).done(function() {
              $('#customerExcel').modal('show');
            });
          });

          $( ".import-excel" ).on( "click", function() {

            $.ajax({
              url: 'excel_controller/do_upload'
            }).done(function() {
              // alert("success");
            });
          });
      </script>
      <script type="text/javascript">

        // $(document).ready(function() {
        //   var interval;

        //   function applyAjaxFileUpload(element) {
        //     $(element).AjaxFileUpload({
        //       action: "excel_controller/do_upload",
        //       onChange: function(filename) {
        //         // Create a span element to notify the user of an upload in progress
        //         var $span = $("<span />")
        //           .attr("class", $(this).attr("id"))
        //           .text("Uploading")
        //           .insertAfter($(this));

        //         $(this).remove();

        //         interval = window.setInterval(function() {
        //           var text = $span.text();
        //           if (text.length < 13) {
        //             $span.text(text + ".");
        //           } else {
        //             $span.text("Uploading");
        //           }
        //         }, 200);
        //       },
        //       onSubmit: function(filename) {
        //         return true;
        //       },
        //       onComplete: function(filename, response) {
        //         alert("import success");
        //         window.clearInterval(interval);
        //         var $span = $("span." + $(this).attr("id")).text(filename + " "),
        //           $fileInput = $("<input />")
        //             .attr({
        //               type: "file",
        //               name: $(this).attr("name"),
        //               id: $(this).attr("id")
        //             });

        //         if (typeof(response.error) === "string") {
        //           $span.replaceWith($fileInput);

        //           applyAjaxFileUpload($fileInput);

        //           alert(response.error);

        //           return;
        //         }

        //         $("<a />")
        //           .attr("href", "#")
        //           .text("x")
        //           .bind("click", function(e) {
        //             $span.replaceWith($fileInput);

        //             applyAjaxFileUpload($fileInput);
        //           })
        //           .appendTo($span);
        //       }
        //     });
        //   }

        //   applyAjaxFileUpload("#demo1");
        // });

      </script>
    </body>
</html>
