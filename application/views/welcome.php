<!DOCTYPE html>
<html>
    <head>
        <title>
            se7en - Dashboard
        </title>
        <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/font-awesome.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/se7en-font.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/isotope.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/jquery.fancybox.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/fullcalendar.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/wizard.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/select2.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/morris.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/datatables.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/datepicker.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/timepicker.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/colorpicker.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/bootstrap-switch.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/bootstrap-editable.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/daterange-picker.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/typeahead.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/summernote.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/ladda-themeless.min.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/social-buttons.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/pygments.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/color/green.css" media="all" rel="alternate stylesheet" title="green-theme" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/color/orange.css" media="all" rel="alternate stylesheet" title="orange-theme" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/color/magenta.css" media="all" rel="alternate stylesheet" title="magenta-theme" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/color/gray.css" media="all" rel="alternate stylesheet" title="gray-theme" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/jquery.fileupload-ui.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>stylesheets/dropzone.css" media="screen" rel="stylesheet" type="text/css" />


        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" charset="utf-8">
        <style>
            .modal-dialog{left: auto;}
            .btn-lg{padding: 4px 10px;}
        </style>
    </head>
    <body class="page-header-fixed bg-3">
        <div class="modal-shiftfix">

            <!-- Navigation -->
            <div class="navbar navbar-fixed-top scroll-hide">
                <?php  $this->load->view('include/top_bar_menu'); ?>
                
                <?php  $this->load->view('include/main_menu'); ?>
            </div>
            <!-- End Navigation -->


            <div class="container-fluid main-content">
                <div class="page-title">
                    <h1>
                        Admin Info
                    </h1>
                </div>
                <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                Delete Dialog
                            </div>
                            <div class="modal-body">
                                Are you sure?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <a href="" class="btn btn-danger danger delete-row" id="remove">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal Change Password Start -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Change Password</h4>
                      </div>
                      <div class="modal-body">
                        <form id="update-password-form" action="/login_controller/update_password/" method="post">
                            <div class="row">
                                <div class="form-group">
                                  <label class="control-label col-md-3">New Password</label>
                                  <div class="col-md-7">
                                    <input class="form-control" placeholder="Password" type="text" id="modal-pass" name="modal-pass">
                                  </div>
                                </div>
                            </div>
                          <!--   <div class="row">
                                <div class="form-group">
                                  <label class="control-label col-md-2">Confirm Password</label>
                                  <div class="col-md-7">
                                    <input class="form-control" placeholder="Confirm Password" type="text" id="modal-pass-conf" name="modal-pass-conf">
                                  </div>
                                </div>
                            </div> -->
                        
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="save-password">Save changes</button>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
                <!-- Modal Change Password End -->

                <!-- DataTables Example -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="widget-container fluid-height clearfix">
                            <div class="heading">
                                <i class="fa fa-table"></i>DataTable with Sorting
                                <?php if($this->session->userdata('permission') != 3){ ?>
                                <a class="btn btn-sm btn-primary-outline pull-right" href="#" id="add-row">
                                    <i class="fa fa-plus"></i>Add row
                                </a>
                                <?php } ?>
                            </div>
                            <div class="widget-content padded clearfix">
                                <table class="table table-bordered table-striped" id="datatable-editable">
                                    <thead>
                                    <th>
                                        First Name
                                    </th>
                                    <th>
                                        Last Name
                                    </th>
                                    <th>
                                        Tel
                                    </th>
                                    <th>
                                        Email
                                    </th>
                                    <th class="sorting">
                                        Password
                                    </th>
                                    <th class="sorting">
                                        Permission
                                    </th>
                                    <?php if($this->session->userdata('permission') != 3){ ?>
                                    <th width="60"></th>
                                    <?php } ?>
                                    <?php if($this->session->userdata('permission') == 1){ ?>
                                    <th width="75"></th>
                                    <?php } ?>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($results as $result) {
                                            ?>
                                            <tr data-id="<?php echo $result->user_id ?>">
                                                <td>
                                                    <?php echo $result->name ?>
                                                </td>
                                                <td>
                                                    <?php echo $result->lastname ?>
                                                </td>
                                                <td>
                                                    <?php echo $result->tel ?>
                                                </td>
                                                <td>
                                                    <?php echo $result->email ?>
                                                </td>
                                                <td>
                                                    <!--<?php echo $result->password ?>-->
                                                    <!-- <button id="pass<?php echo $result->user_id?>" class="pass-user" disabled>change password</button> -->
                                                    <button type="button" id="pass<?php echo $result->user_id?>" class="btn btn-primary btn-lg pass-user" data-toggle="modal" data-target="#myModal" disabled>
                                                      change password
                                                    </button>
                                                </td>
                                                <td>
                                                    <?php echo $result->type ?>
                                                </td>
                                                <?php if($this->session->userdata('permission') != 3){ ?>
                                                <td>
                                                    <a class="edit-row" href="">Edit</a>
                                                </td>
                                                <?php } ?>
                                                <?php if($this->session->userdata('permission') == 1){ ?>
                                                <td>
                                                    <a class="delete-row" href="">Delete</a>
                                                </td>
                                                <?php } ?>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end DataTables Example -->
            </div>



            



        </div>


       

        <!--<?php  $this->load->view('include/style_selector'); ?>-->
        
        <script>
            var permission = <?php echo $permission; ?>;
            
            
            
        </script>



        <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/raphael.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/selectivizr-min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/jquery.mousewheel.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/jquery.vmap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/jquery.vmap.sampledata.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/jquery.bootstrap.wizard.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/fullcalendar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/gcal.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/datatable-editable.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/jquery.easy-pie-chart.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/excanvas.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/jquery.isotope.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/isotope_extras.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/modernizr.custom.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/jquery.fancybox.pack.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/select2.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/styleswitcher.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/wysiwyg.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/typeahead.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/summernote.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/jquery.inputmask.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/jquery.validate.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/bootstrap-fileupload.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/bootstrap-datepicker.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/bootstrap-timepicker.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/bootstrap-colorpicker.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/typeahead.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/spin.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/ladda.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/moment.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/mockjax.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/bootstrap-editable.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/xeditable-demo-mock.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/xeditable-demo.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/address.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/daterange-picker.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/date.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/morris.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/skycons.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/fitvids.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/dropzone.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/main.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>javascripts/respond.js" type="text/javascript"></script>
        <script>
            /*$(function(){
             
             $('.edit-row').click(function(e){
             e.preventDefault();
             var name = $(this).closest('tr').find('input');
             console.log('66666666666');
             console.log(name);
             
             });
             
             $('#add-row').click(function(e){
             e.preventDefault();
             console.log('5555555555');
             
             });
             
             
             
             });*/
        </script>
    </body>
</html>
