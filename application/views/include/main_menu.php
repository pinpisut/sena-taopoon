<div class="container-fluid main-nav clearfix">
    <div class="nav-collapse">
        <ul class="nav">
            <!-- <li>
                <a href="#"><span aria-hidden="true" class="se7en-home"></span>Home</a>
            </li> -->
            <?php if($this->session->userdata('permission') < 3){ ?>
            <li>
                <a href="<?php echo site_url('login_controller/welcome') ?>"><span aria-hidden="true" class="se7en-charts"></span>Admin Info</a>
            </li>
            <?php } ?>
           <!--  <li><a href="<?php //echo site_url('roomtype_controller') ?>">
                    <span aria-hidden="true" class="se7en-feed"></span>Room Type</a>
            </li> -->
            <li><a href="<?php echo site_url('room_controller') ?>">
                    <span aria-hidden="true" class="se7en-star"></span>Room</a>
            </li>
            <!--<li class="dropdown"><a data-toggle="dropdown" href="#">
                <span aria-hidden="true" class="se7en-feed"></span>Room Type<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="<?php echo site_url('roomtype_controller') ?>">Room Type</a>
                  </li>
                </ul>
            </li>-->
            <?php if($this->session->userdata('permission') == 1){ ?>
            <!--<li class="dropdown"><a data-toggle="dropdown" href="#">
                <span aria-hidden="true" class="se7en-star"></span>Tower & Floor<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<?php echo site_url('room_controller/tower') ?>">Add Tower</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('room_controller/floor') ?>">Add Floor</a>
                    </li>
                </ul>
            </li>-->
            <?php } ?>
           <!--  <li><a href="<?php echo site_url('gallery_controller') ?>">
                    <span aria-hidden="true" class="se7en-gallery"></span>Gallery</a>
            </li>
            <li><a href="<?php echo site_url('search_controller') ?>">
                    <span aria-hidden="true" class="se7en-star"></span>Quotation</a>
            </li>-->
            <!--<li><a href="<?php echo site_url('excel_controller') ?>">
                    <span aria-hidden="true" class="se7en-star"></span>Manage Excel</a>
            </li>-->

            <li><a href="<?php echo site_url('login_controller/logout') ?>">
                    <span aria-hidden="true" class="se7en-home"></span>Logout</a>
            </li>
        </ul>
    </div>
</div>
