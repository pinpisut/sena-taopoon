<!DOCTYPE html>
<html>
  <head>
    <title>
      Login
    </title>
    <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>stylesheets/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>stylesheets/font-awesome.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>stylesheets/se7en-font.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url()?>stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />

    <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/raphael.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/jquery.mousewheel.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/jquery.vmap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/jquery.vmap.sampledata.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/jquery.bootstrap.wizard.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/fullcalendar.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/gcal.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/datatable-editable.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/jquery.easy-pie-chart.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/excanvas.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/jquery.isotope.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/modernizr.custom.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/select2.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/styleswitcher.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/wysiwyg.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/jquery.inputmask.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/bootstrap-fileupload.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/bootstrap-timepicker.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/daterange-picker.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/morris.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/skycons.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/fitvids.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/main.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>javascripts/respond.js" type="text/javascript"></script>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
  </head>
  <body class="login1">
    <!-- Login Screen -->
    <form action="<?php echo site_url('login_controller/login_page') ?>" method="post">
      <div class="login-wrapper">
        <div class="login-container">
          <a href="./">
            <img height="42%" src="images/Logo-sena.jpg" />
            <!-- <img width="100%" height="30%" src="images/Logo-sena.jpg" /> -->
          </a>
          <form action="index.html">
            <div class="form-group">
              <input class="form-control" placeholder="Email" type="text" name="email">
            </div>
            <div class="form-group">
              <input class="form-control" placeholder="Password" type="password" name="pwd"><input type="submit" value="&#xf054;">
            </div>
            <div class="form-options clearfix">
              <!--<a class="pull-right" href="#">Forgot password?</a>-->
              <div class="text-left">
                <label class="checkbox"><input type="checkbox"><span>Remember me</span></label>
              </div>
            </div>
          </form>
          <!--<p class="signup">
            Don't have an account yet? <a href="signup1.html">Sign up now</a>
          </p>-->
        </div>
      </div>
    </form>
    <!-- End Login Screen -->
    
  </body>
</html>