<?php
	class Excel_model extends CI_Model {

		function insert($data){

			$this->db->insert('pdf_file', $data);
		}

		function getRoom(){
			// $this->db->select('r.id, r.room_code, t.tower, r.floor, r.size, rt.room_type, r.price, r.discount, r.reservation, r.contract, r.installment, r.balloon, s.status, s.status_id,r.price_sqm,r.gap,r.amount,r.ownership,f.image floorimg,rt.image roomimg');
      // $this->db->from('room r');
      // $this->db->join('tower t', 't.id=r.tower');
      // $this->db->join('room_type rt', 'rt.room_id=r.room_type', 'LEFT');
      // $this->db->join('room_status s', 's.status_id=r.status');
      // $this->db->join('floor f', 'f.floor=r.floor');
      // $this->db->order_by("r.id","asc");
      // $query = $this->db->get();
			$this->db->select('r.id, r.room_code, t.tower, r.floor, r.size, rt.room_type, r.price, r.discount, r.reservation, r.contract, r.installment, r.balloon, s.status, s.status_id,r.price_sqm,r.budget_price,r.amount,r.ownership,r.tfs,r.mb,r.compass, rf.detail, f.floor floorname');
      $this->db->from('room r');
      $this->db->join('tower t', 't.id=r.tower');
      $this->db->join('room_type rt', 'rt.room_id=r.room_type', 'LEFT');
      $this->db->join('room_status s', 's.status_id=r.status');
      $this->db->join('room_function rf', 'rf.id=r.room_function');
      $this->db->join('floor f', 'f.id=r.floor');
      $this->db->order_by("r.id","asc");
      $query = $this->db->get();
			
			return $query;
		}

		function getCustomer(){
			$query = $this->db->get('pdf_file');
			return $query;
		}

	}
?>
