<?php
	class Roomtypemain_model extends CI_Model { 
		function getRoomType(){
			// $query = $this->db->get('room_type');
			// return $query;
			$this->db->select('t.id, t.name, t.size, t.images, t.bedroom, t.bathroom');
				$this->db->from('room_typemain t');
				// $this->db->join('room_function f', 'f.id=t.room_function'); 
				$query = $this->db->get();
	        return $query;
		}

		function getRoomTypeByID($id){
			$this->db->where('id', $id);
			$query = $this->db->get('room_typemain');
			return $query;
		}

		function update($param, $id = 0){
			$this->db->where('id',$id);
			$this->db->update('room_typemain', $param);
		}

		function create($param){
			$this->db->insert('room_typemain', $param);
		}

		function delete($id){
			$this->db->delete('room_typemain', array('id' => $id));
		}

		function getRoomFunc(){
			$query = $this->db->get('room_function');
			return $query;
		}
	}
?>