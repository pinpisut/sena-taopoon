<?php
	class Gallery_model extends CI_Model { 
		function getAll(){
			$query = $this->db->get('gallery');
			return $query;
		}

		function create($param){
			$this->db->insert('gallery', $param);
		}

		function getGallerByID($dataID){
			$this->db->where('id', $dataID);
			$query = $this->db->get('gallery');
			return $query;
		}
		function update($param, $id = 0){
			$this->db->where('id',$id);
			$this->db->update('gallery', $param);
		}

		function delete($id){
			$this->db->delete('gallery', array('id' => $id));
		}

	}
?>