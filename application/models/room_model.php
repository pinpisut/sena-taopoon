<?php
	class Room_model extends CI_Model {

		function getRoom(){
			$this->db->select(
				'r.id, 
				r.room_code, 
				r.room_no,
				r.unit_no,
				t.tower,
				r.floor, 
				r.size, 
				rt.room_type, 
				r.price, 
				s.status, 
				s.abbreviation,
				s.status_id,
				r.compass,
				r.balcony,
				f.floor floorname');
            $this->db->from('room r');
            $this->db->join('tower t', 't.id=r.tower');
            $this->db->join('room_type rt', 'rt.room_id=r.room_type', 'LEFT');
            $this->db->join('room_status s', 's.status_id=r.status');
            $this->db->join('floor f', 'f.id=r.floor');
            $this->db->order_by("r.id","asc");
            $query = $this->db->get();

			//$query = $this->db->get('room');
			return $query;
		}

		function getRoomcode($roomcode){
			$this->db->where('room_code', $roomcode);
			$query = $this->db->get('room');
			return $query;
		}

		function getFloorByID($id){
			$this->db->where('id', $id);
			$query = $this->db->get('floor');
			return $query;
		}

		function update($param, $id = 0){
			$this->db->where('id',$id);
			$this->db->update('floor', $param);
		}

		function getTower(){
			$query = $this->db->get('tower');
			return $query;
		}

		function getTowerWhere($id){
			$this->db->where('id', $id);
			$query = $this->db->get('tower');
			return $query;
		}

		function insertTower($tower_name){
			$data = array(
			   'tower' => $tower_name
			);

			$this->db->insert('tower', $data);
			return $this->db->insert_id();
		}

		function updateTower($id, $tower){
			$data = array(
				'id' => $id,
				'tower' => $tower
				);


			$this->db->where('id', $id);
			$this->db->update('tower', $data);
		}

		function deleteTower($id){
			$this->db->delete('tower',array('id'=>$id));
		}

		function getFloor($tower_id){
			$this->db->select("floor.id,tower.tower,floor.floor,floor.tower tower_id");
			$this->db->from('floor');
			$this->db->where('floor.tower', $tower_id);
			$this->db->join('tower', 'tower.id = floor.tower');
			$query = $this->db->get();


			//$query = $this->db->get_where('floor', array('tower' => $tower_id));
			return $query;
		}

		function insertFloor($tower, $floor){
			$data = array(
				'tower' => $tower,
				'floor' => $floor
			);

			$this->db->insert('floor',$data);
			return $this->db->insert_id();
		}

		function updateFloor($id, $floor){
			$data = array(
				'id' => $id,
				'floor' => $floor
			);

			$this->db->where('id',$id);
			$this->db->update('floor',$data);
		}

		function deleteFloor($floor_id){
			$this->db->delete('floor',array('id'=>$floor_id));
		}

		function getListFloor($tower_name){
			$this->db->select('id, floor');
			$this->db->from('floor');
			$this->db->where('tower', $tower_name);
			$query = $this->db->get();
			return $query;
		}

		function getListFloorFirst(){
			$this->db->select('id, floor');
			$this->db->from('floor');
			$this->db->where('tower', 1);
			$query = $this->db->get();
			return $query;
		}

		function getRoomType(){
			$this->db->select('room_id, room_type');
			$query = $this->db->get('room_type');

			return $query;
		}

		function getStatus(){
			$query = $this->db->get('room_status');
			return $query;
		}

		function insertRoom($data){

			$this->db->insert('room',$data);
			return $this->db->insert_id();
		}

		function updateRoom($id, $data){

			$this->db->where('id',$id);
			$this->db->update('room',$data);
		}

		function deleteRoom($room_id){
			$this->db->delete('room',array('id'=>$room_id));
		}

		function getStatusForXML(){
			$this->db->select('room_code, status');
			$query = $this->db->get('room');

			return $query;
		}

		function getRoomTypeForXML(){
			$query = $this->db->get('room_type');

			return $query;
		}

		function getGalleryForXML(){
			$query = $this->db->get('gallery');
			return $query;
		}

		function getFloorAll(){
			$this->db->select('floor.id, floor.floor, floor.image, tower.tower name');
			$this->db->from('floor');
			$this->db->join('tower', 'tower.id = floor.tower');

			$query = $this->db->get();


			return $query;
		}
		function getRoomFunction(){
			$this->db->select('id, detail');
			$query = $this->db->get('room_function');

			return $query;
		}
		function getFloorByTower($tower, $floor){
			if($tower == 'A'){
				$tower_id = 1;
			}elseif ($tower == 'B') {
				$tower_id = 2;
			}else{
				$tower_id = 3;
			}
			$this->db->select("id");
			$this->db->from('floor');
			$this->db->where('floor.tower', $tower_id);
			$this->db->where('floor.floor', $floor);
			$query = $this->db->get();


			//$query = $this->db->get_where('floor', array('tower' => $tower_id));
			return $query;
		}

		// function insertNewType(){
		// 	$this->db->select('DISTINCT(type)');
		//    $this->db->from('type_all');
		//    $query = $this->db->get();

		//    return $query;
		// }

		// function insertNewType2($tower, $floor){
		// 	$data = array(
		// 		'tower' => $tower,
		// 		'floor' => $floor
		// 	);

		// 	$this->db->insert('floor',$data);
		// }

		function insertTypeAll($data){
			$this->db->insert('type_all', $data);
		}


	}
?>
