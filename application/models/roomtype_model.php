<?php
	class Roomtype_model extends CI_Model { 
		function getRoomType(){
			// $query = $this->db->get('room_type');
			// return $query;
			// $this->db->select('t.room_id, t.room_type, t.room_function, t.size, t.image, t.typemain_id , tm.id, tm.name');
			// 	$this->db->from('room_type t');
			// 	$this->db->join('room_typemain tm', 't.typemain_id=tm.id'); 
				$query = $this->db->get('room_type');
	        return $query;
		}

		function getRoomTypeByID($id){
			$this->db->where('room_id', $id);
			$query = $this->db->get('room_type');
			return $query;
		}

		function update($param, $id = 0){
			$this->db->where('room_id',$id);
			$this->db->update('room_type', $param);
		}

		function create($param){
			$this->db->insert('room_type', $param);
		}

		function delete($id){
			$this->db->delete('room_type', array('room_id' => $id));
		}

		function getRoomFunc(){
			$query = $this->db->get('room_typemain');
			return $query;
		}
	}
?>