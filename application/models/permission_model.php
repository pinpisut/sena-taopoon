<?php

class Permission_model extends CI_Model {

    function all() {
    	$perm = $this->session->userdata('permission');
    	if($perm == 1){
    		$query = $this->db->from('permission')->get();
    	}else if($perm > 1){
    		$this->db->from('permission');
    		$this->db->where('permission_id != 1');
    		$query = $this->db->get();
    	}
        
        return $query;
    }

}

?>