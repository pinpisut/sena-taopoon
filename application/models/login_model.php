<?php
	class Login_Model extends CI_Model { 

	   function get_info_login($email,$password){
				$query = $this->db->get_where('user', array('email' => $email, 'password' => $password));
	        return $query;
	   }

	   function get_all_info(){
	   		//$query = $this->db->get('user');
	   		$this->load->library('session');
			$email = $this->session->userdata('email');
			$password = $this->session->userdata('password');

	   		//$email = 'kik@gmail.com';
	   		//$password = '81dc9bdb52d04dc20036dbd8313ed055';

	  //  		$check_permission = $this->db->get_where('user', array('email' => $email, 'password' => $password));

	  //  		foreach ($check_permission->result() as $cp)
			// {
			//    $permission = $cp->permission;
			// }

			// if(isset($permission) && $permission == "1"){
			// 	//$query = $this->db->get('user');	
			// 	$this->db->select('user.user_id,user.name,user.lastname,user.tel,user.email,user.password,permission.type');
			// 	$this->db->from('user');
			// 	$this->db->join('permission','user.permission=permission.permission_id');
			// 	$query=$this->db->get();
			
			// }else{
			// 	$this->db->select('user.user_id,user.name,user.lastname,user.tel,user.email,user.password,permission.type');
			// 	$this->db->from('user');
			// 	$this->db->join('permission','user.permission=permission.permission_id');
			// 	$this->db->where('user.permission','2');
			// 	$query=$this->db->get();
				
			// }

			$this->db->select("*");
			$this->db->from('user');
			$this->db->join('permission','user.permission=permission.permission_id');
			$query=$this->db->get();

	        return $query;
	  }

	  function insert_user($data){

		$this->db->insert('user', $data); 
		return $this->db->insert_id();
	  }


	  function update_user($data, $id){
	  	
	  	$this->db->where('user_id', $id);
		$this->db->update('user', $data); 
	  }

	  function delete_user($user_id){
	  	$this->db->delete('user', array('user_id' => $user_id)); 
	  }

	  function getUserByID($id){
	  		$this->db->where('user_id', $id);
			$query = $this->db->get('user');
			return $query;
	  }


	} 
?>