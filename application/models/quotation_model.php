<?php
	class Quotation_model extends CI_Model { 

		function insert($data){

			$this->db->insert('pdf_file', $data); 
		}

		function getQuotation($roomcode){
			$this->db->select('r.id, r.room_code, t.tower, r.floor, r.size, rt.room_type, r.price, r.discount, r.reservation, r.contract, r.installment, r.balloon, s.status, s.status_id,r.price_sqm,r.gap,r.amount,r.ownership,f.image floorimg,rt.image roomimg');
            $this->db->from('room r'); 
            $this->db->where('room_code',$roomcode);
            $this->db->join('tower t', 't.id=r.tower');
            $this->db->join('room_type rt', 'rt.room_id=r.room_type', 'LEFT');
            $this->db->join('room_status s', 's.status_id=r.status');
            $this->db->join('floor f', 'f.floor=r.floor');
            $this->db->order_by("r.room_code","asc");
            $query = $this->db->get();
			return $query;
		}

	}
?>