<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function room_status() {

        $output = isset($_GET['output']) ? $_GET['output'] : "xml";

        $this->load->database();
        $this->load->model('Room_model');
        $query = $this->Room_model->getRoom();

        if ($output == "xml") {

            $response = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
                        .'<Rooms xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
            foreach ($query->result() as $room):
                $response .= $this->_generateXMLRoomStatus($room);
            endforeach;
            $response .= '</Rooms>';

            header("Content-type: text/xml; charset=utf-8");
            echo $response;

        } else if ($output == "json") {

            $response = array();
            foreach ($query->result() as $room):
                $response[] = $this->_generateJSONRoomStatus($room);
            endforeach;

            header("Content-type: application/json; charset=utf8");
            echo json_encode($response);
            die;
        }
    }



    private function _generateXMLRoomStatus($roomModel)
    {
        return '<Room>'
                .'    <roomcode>'.$roomModel->room_id.'</roomcode>'
                .'   <roomstatus>'.$roomModel->status_id.'</roomstatus>'
                .'</Room>';
    }

    private function _generateJSONRoomStatus($roomModel)
    {
        return array(
            'roomcode' => $roomModel->room_id,
            'roomstatus' => $roomModel->status_id
            );
    }
}



