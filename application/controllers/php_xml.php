<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Php_xml extends CI_Controller {

    function index() {
        exit();
        if (file_exists('xml/room_sena.json')) {
            $string = file_get_contents('xml/room_sena.json');
            $json_a = json_decode($string, true);
            // echo "<pre />";

            $results = $this->db->get('room_type')->result();
            $db_floor = $this->db->get('floor')->result();
            $db_room_status = $this->db->get('room_status')->result();

            $roomTypeArray = array();
            foreach ($results as $value) {
                $roomTypeArray[$value->room_type] = $value->room_id;
            }

            // print_r($roomTypeArray);

            $dbFloorArray = array();
            foreach ($db_floor as $value) {
                $dbFloorArray[$value->floor] = $value->id;
            }
            
            $dbStatusArray = array();
            foreach ($db_room_status as $value) {
                $dbStatusArray[$value->abbreviation] = $value->status_id;
            }

            // exit();

            foreach ($json_a as $key => $value) {
                foreach ($value as $rn => $field){
                    $param = array(
                        'room_code' => (int) $rn,                        
                        'room_no' => $field['no'],
                        'unit_no' => $field['unitno'],
                        'tower' => 1,
                        'floor' => (int) $dbFloorArray[$field['floor']],
                        'size' => (float) $field['size'],
                        'room_type' => $roomTypeArray[$field['type']],
                        'price' => (int)$field['price']['total'],
                        'compass' => $field['view'],
                        'balcony' => $field['balcony'],
                        'status' => $dbStatusArray[$field['status']]
                     );

                    // print_r($param);
                    $this->db->insert('room', $param);

                    // echo "key: $rn" . "<br/>";
                    // echo "no: " . $field['no'] . "<br/>";
                    // echo "floor: " . $field['floor'] . "<br/>";
                    // echo "unitno: " . $field['unitno'] . "<br/>";
                    // echo "type: " . $field['type'] . "<br/>";
                    // echo "size: " . $field['size'] . "<br/>";
                    // echo "view: " . $field['view'] . "<br/>";
                    // echo "price: " . $field['price']['total'] . "<br/>";
                    // echo "status: " . $field['status'] . "<br/>";
                    // echo "******************************" . "<br/>";
                    // exit();
                }
                
                // echo $value['size'];
            }
            // exit();
        }
    }

    function roomtype() {
        if (file_exists('xml/room_sena.json')) {
            $string = file_get_contents('xml/room_sena.json');
            $json_a = json_decode($string, true);            
            foreach ($json_a as $key => $value) {
                foreach ($value as $rn => $field){
                    $param = array(
                        'type' => (string)$field['type']
                     );
                    $this->db->insert('type_all', $param);
                }
            }
        }
    }

    function gallery(){
        if (file_exists('room.xml')) {
            $xml = simplexml_load_file('images.xml');

            foreach ($xml->children() as $key => $value) {

                $param = array(
                    'type' => (string)$value->type,
                    'url' => (string)$value->url,
                    'instance_name' => (string)$value->instance_name
                 );
                $this->db->insert('gallery', $param);
                // echo "<pre/>";
                // print_r($param);
            }
        }
    }

    function sheetData($sheet) {
      $re = '<table>';     // starts html table

      $x = 5;
      while($x <= $sheet['numRows']) {
        $inserArr = [];
        $re .= "<tr>\n";
        $y = 1;
        while($y <= $sheet['numCols']) {
          $cell = isset($sheet['cells'][$x][$y]) ? $sheet['cells'][$x][$y] : '';
          $re .= " <td>$y: $cell || </td>\n";
          if($y == 1){
            $inserArr['room_code'] = $cell;
          }
          else if($y == 2){
            $inserArr['price_sqm'] = $cell;
          }
          else if($y == 3){
            $inserArr['size'] = $cell;
          }
          else if($y == 4){
            $inserArr['price'] = $cell;
          }else if($y == 5){
            $inserArr['reservation'] = $cell;
          }else if($y == 6){
            $inserArr['contract'] = $cell;
          }
          $y++;
        }
        // echo "<pre>";
        // print_r($inserArr);
        // $this->db->update('room', $inserArr, array('room_code' => $inserArr['room_code']));
        $re .= "</tr>\n";
        $x++;
      }

      return $re .'</table>';     // ends and returns the html table
    }

    function excel(){
        include 'excel_reader.php';     // include the class

        $this->load->library('excel');

        $this->load->database();
        $this->load->model('Room_model');

        $inputFileName = "knightsbridge_data.xls";
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($inputFileName);

        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
        $highestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();

        echo "amount:: ".$highestRow."<br/>";

        $roomtype_model = $this->Room_model->getRoomType()->result();
        $roomfunction_model = $this->Room_model->getRoomFunction()->result();
        $tower_model = $this->Room_model->getTower()->result();

        // echo "<pre/>";
        // print_r($roomtype_model);
        // exit();

        for( $i=2 ; $i<=$highestRow ; $i++ )
          {
            $room_no = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
            $building = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
            $floor = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
            $price_sqm = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
            $area = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
            $price = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
            $room_type = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
            $tfs = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
            $type_def = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
            $compass = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
            $zone = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
            $mb = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
            $discount = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
            $budget_price = $objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
            $booking = $objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
            $contract = $objWorksheet->getCellByColumnAndRow(15,$i)->getValue();

            $floor_model = $this->Room_model->getFloorByTower($building,$floor)->result();

              foreach ($roomtype_model as $key => $value) {
                  if($value->room_type == $type){
                    $roomtype_id = $value->room_id;
                  }
              }
              foreach ($roomfunction_model as $key => $rfc) {
                  if($rfc->detail == $type_def){
                    $room_function = $rfc->id;
                  }
              }
              foreach ($tower_model as $key => $tower) {
                  if($tower->tower == $building){
                    $towerm = $tower->id;
                  }
              }
              foreach ($floor_model as $key => $fm) {
                  $floorm = $fm->id;
              }

              // print_r($floor_model);
              // exit();

              // echo $roomcode."  ".$price_per_sqm."  ".$price."  ".$roomtype_id."<br/>";

            // $query = $this->Room_model->getRoomcode($roomcode)->result();
            // $qroomcode = $query[0]->room_code;

            // if($qroomcode == $roomcode){
            //   $data_insert = array(
            //         // 'room_code' => $roomcode,
            //         'price_sqm' => $price_per_sqm,
            //         'size' => $area,
            //         'price' => $price,
            //         'gap' => $gap,
            //         'reservation' => $reservation,
            //         'contract' => $contract,
            //         'room_type' => $roomtype_id,
            //         'balloon' => $balloon,
            //         'installment' => $installment,
            //         'amount' => $amount,
            //         'ownership' => $ownership,

                    // 'tower' => 1,
                    // 'floor' => $floor,
                    // 'status' => 1,

            //  );
              // echo $query[0]->room_code."<br/>";
             // $querys = $this->Room_model->updateRoom($query[0]->id,$data_insert);
           // }else{
              $data_insert = array(
                    'room_code' => $room_no,
                    'tower' => $building,
                    'floor' => $floorm,
                    'price_sqm' => $price_sqm,
                    'size' => $area,
                    'price' => $price,
                    'room_type' => $roomtype_id,
                    'tfs' => $tfs,
                    'room_function' => $room_function, //type_def
                    'compass' => $compass,
                    'zone' => $zone,
                    'mb' => $mb,
                    'discount' => $discount,
                    'budget_price' => $budget_price,
                    'reservation' => $booking,
                    'contract' => $contract,

                    // 'gap' => $gap,
                    // 'reservation' => $reservation,
                    // 'contract' => $contract,

                    // 'balloon' => $balloon,
                    // 'installment' => $installment,
                    // 'amount' => $amount,
                    // 'ownership' => $ownership,


                    // 'floor' => $floor,
                    'status' => 1,

              );
              $querys = $this->Room_model->insertRoom($data_insert);
            //}
            //   $query = $this->Room_model->insertRoom($data_insert);

          }

    }

    function exportexcel(){
      // Create your database query
        $this->load->database();
        $this->load->model('Room_model');
        $query = $this->Room_model->getRoom()->result();

        // echo "<pre/>";
        // print_r($query);
        // exit();

        include 'excel_reader.php';     // include the class

        $this->load->library('excel');

        // Instantiate a new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $sheet = $objPHPExcel->getActiveSheet();

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $styleArray = array(
              'borders' => array(
                  'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                  )
              )
          );

        // Set the active Excel worksheet to sheet 0
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->mergeCells('A2:C2');
        $sheet->getStyle("A2:C2")->applyFromArray($style);
        $objPHPExcel->getActiveSheet()->SetCellValue('A2', 'Kensington Kaset Room Data');

        $sheet->getStyle("A4:C4")->applyFromArray($style);
        // $sheet->getStyle("L")->applyFromArray($style);
        $sheet->getStyle("A4:C4")->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);

        $objPHPExcel->getActiveSheet()->SetCellValue('A4', 'Room Code');
        $objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Status');
        $objPHPExcel->getActiveSheet()->SetCellValue('C4', 'MB.');

        // Initialise the Excel row number
        $rowCount = 5;
        // Iterate through each result from the SQL query in turn
        // We fetch each database result row into $row in turn
        foreach ($query as $result) {

            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$rowCount, $result->room_code, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $result->status);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $result->mb);

            $rowCount++;
        }

        // Instantiate a Writer to create an OfficeOpenXML Excel .xls file
        // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename='room_data.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('php://output');
    }

}

?>
