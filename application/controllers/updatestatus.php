<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Updatestatus extends CI_Controller {
    
    public function index() {
        $room_code = $this->input->post('room_code');
        $rstatus = strtoupper($this->input->post('room_status'));

        if($rstatus == 'A'){
            $room_status = 1;
        }elseif ($rstatus == 'B') {
            $room_status = 2;
        }elseif ($rstatus == 'S') {
            $room_status = 3;
        }elseif ($rstatus == 'F') {
            $room_status = 4;
        }else{
            $room_status = null;
        }

        $this->load->database();
        $this->load->model('Updatestatus_model');

        if($room_code==''){
            echo "room_code is null."; 
        }elseif ($room_status==null) {
            echo "room_status is A, B, S or F";
        }else {
            $data = array(
                'status' => $room_status
            );

            $this->Updatestatus_model->update($room_code, $data);

            //START GEN XML
            $this->load->model('Room_model');
            $queryRoom = $this->Room_model->getRoom();
            $queryStatus = $this->Room_model->getStatusForXML();

            $this->load->helper('file');

            roomExportXml($queryRoom, 'folder');
            roomStatusExportXml($queryStatus, 'folder');
            //END GEN XML

            echo "success";            
        }

        // $response = array(
        //     'masage' => $msg
        // );
        // echo json_encode($response);

        
        // echo $room_code." : ".$room_status;
    }
}