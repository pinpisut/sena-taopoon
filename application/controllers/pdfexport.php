<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pdfexport extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		$this->load->model('Quotation_model');
        $query = $this->Quotation_model->getQuotation($_GET['id'])->result();

        // echo "<pre/>";
        // echo $query[0]->room_code;
        // print_r($query);
        // exit();

		$this->load->library('Pdf');

		$pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
		// $pdf->SetAuthor('Refulz PHP');		 
		$pdf->SetTitle('Ouatation');		 
		// $pdf->SetSubject('Refulz PDF Example');		 
		// $pdf->SetKeywords('refulz, TCPDF, PDF');
		$pdf->SetFont('freeserif', '', 14, '', true);
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
	 
		$pdf->AddPage('L');
		
    	$data['query'] = $query;
		

		// Set some content to print
		$html = $this->load->view('pdf_view', $data, true);
		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->lastPage();		

		$filename = $query[0]->room_code.".pdf";
		 
		$pdf->Output( $filename, 'I' );

	}
}



// public function index2()
// 	{
// 		// echo "555";
// 		// exit();

// 		$data = [];
// 		//load the view and saved it into $html variable
// 		$original=$this->load->view('pdf_view', $data, true);
// 		$html = mb_convert_encoding($original, 'UTF-8', 'windows-1252');
// 		// $html = iconv('UTF-8', 'windows-1252', $original);
// 		// $html = iconv("UTF-8","UTF-8//IGNORE",$original);

//         //this the the PDF filename that user will get to download
// 		$pdfFilePath = "output_pdf_name.pdf";

//         //load mPDF library
// 		$this->load->library('m_pdf');

// 		$this->m_pdf->pdf->AddPage('L'); // margin footer
// 		$this->m_pdf->pdf->allow_charset_conversion=true;
// 		$this->m_pdf->pdf->charset_in='UTF-8';


//        //generate the PDF from the given html
// 		$this->m_pdf->pdf->WriteHTML($html);

//         //download it.
// 		$this->m_pdf->pdf->Output($pdfFilePath, "I");	 //F D I

// 		// redirect("/codeignier-mpdf-master/$pdfFilePath.pdf");	


// 	}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */