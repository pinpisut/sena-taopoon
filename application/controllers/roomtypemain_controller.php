<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Roomtypemain_controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        if (!$this->session->userdata('email')||!$password = $this->session->userdata('password'))
        {
            $logout = base_url();
            redirect($logout);
        }
    }
    
    public function index() {
        $this->load->database();
        $this->load->model('Roomtypemain_model');
        $query = $this->Roomtypemain_model->getRoomType();

        $list_roomfunction = $this->Roomtypemain_model->getRoomFunc()->result();

        $data['results'] = $query->result();
        $data['list_func'] = json_encode($list_roomfunction);

        // echo "<pre/>";
        // print_r($query->result());
        // exit();

        $this->load->view('roomtypeMain/roomtypeMain',$data);
    }

    public function updateImg(){
        $this->load->model('Roomtypemain_model');  

        $dataID = $this->input->post('id');
        $query = $this->Roomtypemain_model->getRoomTypeByID($dataID);      

        $row = $query->row();
        $image = $row->images;
        // $url_detail = $row->url_detail;

        if($this->input->post('type') == 'img'){
            if(file_exists($image)){
                unlink($image);
            }
        }
        // else{
        //     if(file_exists($url_detail)){
        //         unlink($url_detail);
        //     }
        // }

        if($this->input->post('type') == 'img'){
            $pathUrl = 'Content/images/room/';
            $config = array(
                'upload_path'   => $pathUrl,
                'allowed_types' => 'gif|jpg|png',
                'max_size'      => '10000',
                'max_width'     => '102400',
                'max_height'    => '76008',
                'encrypt_name'  => false,
            );      

        }else{

            $pathUrl = 'Content/images/Gallary/';
            $config = array(
                'upload_path'   => $pathUrl,
                'allowed_types' => 'gif|jpg|png',
                'max_size'      => '10000',
                'max_width'     => '102400',
                'max_height'    => '76008',
                'encrypt_name'  => false,
            );  
        }

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            echo "fail";
            echo $this->upload->display_errors();
        } else {
            $upload_data = $this->upload->data();
            $url = $upload_data['file_name'];
        }


        if($this->input->post('type') == 'img'){
            $param = array(
                'images' => $pathUrl.$url
            );
        }
        // else{
        //     $param = array(
        //         'url_detail' => $pathUrl.$url
        //     );
        // }

        $this->Roomtypemain_model->update($param, $dataID);

        /****** Gen XML File Start *******/
        $query = $this->Roomtypemain_model->getRoomType();

        $this->load->helper('file');
        roomTypeMainExportXml($query, 'folder');
        /****** Gen XML File End *******/

        echo json_encode(array('img_path' => base_url().'/'.$pathUrl.$url));
    }

    public function updateData(){
        $this->load->model('Roomtypemain_model');

        $data = $this->input->post();
        $id = $this->input->post('id');
        $param = array(
            'id' => $data['id'], 
            'name' => $data['name'], 
            'size' => $data['size'],
            'bedroom' => $data['bedroom'],
            'bathroom' => $data['bathroom']
            //'direction' => $this->input->post('direction')
            );

        $this->Roomtypemain_model->update($param, $id);

        /****** Gen XML File Start *******/
        $query = $this->Roomtypemain_model->getRoomType();

        $this->load->helper('file');
        roomTypeMainExportXml($query, 'folder');
        /****** Gen XML File End *******/
    }

    public function insert(){
        $data = $this->input->post();
        // echo $_FILES['urlroom']."<br/>";
        // echo $_FILES['urldetail']."<br/>";         

        if($_FILES['urlroom']){

            $pathUrl = 'Content/images/room/';
            $config = array(
                'upload_path'   => $pathUrl,
                'allowed_types' => 'gif|jpg|png',
                'max_size'      => '10000',
                'max_width'     => '102400',
                'max_height'    => '76008',
                'encrypt_name'  => false,
            );                        

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('urlroom')) {
                //echo "fail";
                $paramImgRoom = "";
            } else {
                $upload_data = $this->upload->data();
                $url = $upload_data['file_name'];
                
                $paramImgRoom = $pathUrl.$url;
            }
        }

        $param = array(
            'name' => $data['roomtype'],
            'bedroom' => $data['Bedroom'],
            'bathroom' => $data['Bathroom'],
            'size' => $data['size'],            
            'images' => $paramImgRoom
        );

        $this->load->model('Roomtypemain_model');  
        $this->Roomtypemain_model->create($param);

        /****** Gen XML File Start *******/
        $query = $this->Roomtypemain_model->getRoomType();

        $this->load->helper('file');
        roomTypeMainExportXml($query, 'folder');
        /****** Gen XML File End *******/

        redirect('/roomtypemain_controller', 'refresh');
    }

    public function delete(){
        $id = $this->input->post('id');

        $this->load->model('Roomtypemain_model');  
        $this->Roomtypemain_model->delete($id);

        /****** Gen XML File Start *******/
        $query = $this->Roomtypemain_model->getRoomType();

        $this->load->helper('file');
        roomTypeMainExportXml($query, 'folder');
        /****** Gen XML File End *******/
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */