<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Roomtype_controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        if (!$this->session->userdata('email')||!$password = $this->session->userdata('password'))
        {
            $logout = base_url();
            redirect($logout);
        }
    }
    
    public function index() {
        $this->load->database();
        $this->load->model('Roomtype_model');
        $query = $this->Roomtype_model->getRoomType();

        $list_roomfunction = $this->Roomtype_model->getRoomFunc()->result();

        $data['results'] = $query->result();
        $data['list_func'] = json_encode($list_roomfunction);

        // echo "<pre/>";
        // print_r($query->result());
        // exit();

        $this->load->view('roomtype/roomtype',$data);
    }

    public function updateImg(){
        $this->load->model('Roomtype_model');  

        $dataID = $this->input->post('id');
        $query = $this->Roomtype_model->getRoomTypeByID($dataID);      

        $row = $query->row();
        $image = $row->image;
        // $url_detail = $row->url_detail;

        if($this->input->post('type') == 'img'){
            if(file_exists($image)){
                unlink($image);
            }
        }
        // else{
        //     if(file_exists($url_detail)){
        //         unlink($url_detail);
        //     }
        // }

        if($this->input->post('type') == 'img'){
            $pathUrl = 'Content/images/room/';
            $config = array(
                'upload_path'   => $pathUrl,
                'allowed_types' => 'gif|jpg|png',
                'max_size'      => '10000',
                'max_width'     => '102400',
                'max_height'    => '76008',
                'encrypt_name'  => false,
            );      

        }else{

            $pathUrl = 'Content/images/Gallary/';
            $config = array(
                'upload_path'   => $pathUrl,
                'allowed_types' => 'gif|jpg|png',
                'max_size'      => '10000',
                'max_width'     => '102400',
                'max_height'    => '76008',
                'encrypt_name'  => false,
            );  
        }

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            echo "fail";
            echo $this->upload->display_errors();
        } else {
            $upload_data = $this->upload->data();
            $url = $upload_data['file_name'];
        }


        if($this->input->post('type') == 'img'){
            $param = array(
                'image' => $pathUrl.$url
            );
        }else{
            $param = array(
                'url_detail' => $pathUrl.$url
            );
        }

        $this->Roomtype_model->update($param, $dataID);

        /****** Gen XML File Start *******/
        $query = $this->Roomtype_model->getRoomType();

        $this->load->helper('file');
        roomTypeExportXml($query, 'folder');
        /****** Gen XML File End *******/

        echo json_encode(array('img_path' => base_url().'/'.$pathUrl.$url));
    }

    public function updateData(){
        $this->load->model('Roomtype_model');

        $data = $this->input->post();
        $id = $this->input->post('id');
        $param = array(
            'room_type' => $data['room_type'], 
            'room_function' => $data['room_function'], 
            'size' => $data['size'], 
            'typemain_id' => $data['typemain']
            );

        $this->Roomtype_model->update($param, $id);

        /****** Gen XML File Start *******/
        $query = $this->Roomtype_model->getRoomType();

        $this->load->helper('file');
        roomTypeExportXml($query, 'folder');
        /****** Gen XML File End *******/
    }

    public function insert(){
        $data = $this->input->post();
        // echo $_FILES['urlroom']."<br/>";
        // echo $_FILES['urldetail']."<br/>";         

        if($_FILES['urlroom']){

            $pathUrl = 'Content/images/room/';
            $config = array(
                'upload_path'   => $pathUrl,
                'allowed_types' => 'gif|jpg|png',
                'max_size'      => '10000',
                'max_width'     => '102400',
                'max_height'    => '76008',
                'encrypt_name'  => false,
            );                        

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('urlroom')) {
                //echo "fail";
                $paramImgRoom = "";
            } else {
                $upload_data = $this->upload->data();
                $url = $upload_data['file_name'];
                
                $paramImgRoom = $pathUrl.$url;
            }
        }

        $param = array(
            'room_type' => $data['roomtype'],
            'room_function' => $data['inprogram'],
            'size' => $data['size'],
            'typemain_id' => $data['roomfunction'],
            'image' => $paramImgRoom
        );

        $this->load->model('Roomtype_model');  
        $this->Roomtype_model->create($param);

        /****** Gen XML File Start *******/
        $query = $this->Roomtype_model->getRoomType();

        $this->load->helper('file');
        roomTypeExportXml($query, 'folder');
        /****** Gen XML File End *******/

        redirect('/roomtype_controller', 'refresh');
    }

    public function delete(){
        $id = $this->input->post('id');

        $this->load->model('Roomtype_model');  
        $this->Roomtype_model->delete($id);

        /****** Gen XML File Start *******/
        $query = $this->Roomtype_model->getRoomType();

        $this->load->helper('file');
        roomTypeExportXml($query, 'folder');
        /****** Gen XML File End *******/
    }

    public function specialXmlRoomtype(){
        /****** Gen XML File Start *******/
        $this->load->model('Roomtype_model');  
        $query = $this->Roomtype_model->getRoomType();

        $this->load->helper('file');
        roomTypeExportXml($query, 'folder');
        /****** Gen XML File End *******/
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */