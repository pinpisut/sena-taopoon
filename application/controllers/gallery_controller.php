<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gallery_controller extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        if (!$this->session->userdata('email')||!$password = $this->session->userdata('password'))
        {
            $logout = base_url();
            redirect($logout);
        }
    }

    public function index() {
         $this->load->model('gallery_model');
         $query = $this->gallery_model->getAll();

        // echo '<pre/>';
        // print_r($query->result());
        // exit();

         $data['results'] = $query->result();


        // $this->load->library('imageupload');

        $this->load->view('gallery/gallery', $data);

    }

    public function createGallery(){
        $this->load->model('gallery_model');  
        // echo "<pre/>";      
        // print_r($_POST);
        

        $dataID = $this->input->post('id');
        $query = $this->gallery_model->getGallerByID($dataID);      
        //print_r($query->result());
        //exit();

        $row = $query->row();
        $url = $row->url;
        $thumbnail = $row->thumbnail;
        $type = $row->type;

        if($this->input->post('type') == 'img'){
            if(file_exists($url)){
                unlink($url);
            }
        }else{
            if(file_exists($thumbnail)){
                unlink($thumbnail);
            }
        }
        //echo $url;
        // $this->load->library('imageupload');

        $data = $this->input->post();

         // echo 'caption : '.$data['caption'].'<br>';
         // echo 'gallery type : '.$data['galleryType'].'<br>';
         // echo 'instance name : '.$data['ins_name'].'<br>';
        if($this->input->post('type') == 'img'){
            $typeImage = $type;

            if($typeImage == "Gallary"){
                $pathUrl = 'Content/images/Gallary/';
                $config = array(
                    'upload_path'   => $pathUrl,
                    'allowed_types' => 'gif|jpg|png',
                    'max_size'      => '10000',
                    'max_width'     => '102400',
                    'max_height'    => '76008',
                    'encrypt_name'  => false,
                );            
            }else{
                $pathUrl = 'Content/images/furniture/';
                $config = array(
                    'upload_path'   => $pathUrl,
                    'allowed_types' => 'gif|jpg|png',
                    'max_size'      => '10000',
                    'max_width'     => '102400',
                    'max_height'    => '76008',
                    'encrypt_name'  => false,
                );            
            }

            
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                echo "fail";
            } else {
                $upload_data = $this->upload->data();
                //echo "success";
                $url = $upload_data['file_name'];
            }

        }else{

            $pathUrl = 'Content/images/Thumbnail/';
            $config = array(
                'upload_path'   => $pathUrl,
                'allowed_types' => 'gif|jpg|png',
                'max_size'      => '10000',
                'max_width'     => '102400',
                'max_height'    => '76008',
                'encrypt_name'  => false,
            );  
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                echo "fail";
            } else {
                $upload_data = $this->upload->data();
                //echo "success";
                $url = $upload_data['file_name'];
            }

        }
        //exit();
        

        // $param = array(
        //     'type' => $data['galleryType'], 
        //     'url' => $pathUrl.$url,
        //     'thumbnail' => $pathUrl.$url,
        //     'name' => $data['caption'],
        //     'instance_name' => $data['ins_name']
        //     );

        if($this->input->post('type') == 'img'){
            $param = array(
                'url' => $pathUrl.$url
            );
        }else{
            $param = array(
                'thumbnail' => $pathUrl.$url
            );
        }

        //$this->gallery_model->create($param);
        $this->gallery_model->update($param, $dataID);

        /********** Gen XML Start *************/
        $query = $this->gallery_model->getAll();

        $this->load->helper('file');
        galleryExportXml($query, 'folder');
        /********** Gen XML End *************/

        echo json_encode(array('img_path' => base_url().'/'.$pathUrl.$url));
        // redirect('/gallery_controller', 'refresh');
    }

    public function insert(){

        $data = $this->input->post();

        $typeImage = $this->input->post('galleryType');

        //if($_FILES['userfile']){
            if($typeImage == "Gallary"){
                $pathUrl = 'Content/images/Gallary/';
                $config = array(
                    'upload_path'   => $pathUrl,
                    'allowed_types' => 'gif|jpg|png',
                    'max_size'      => '10000',
                    'max_width'     => '102400',
                    'max_height'    => '76008',
                    'encrypt_name'  => false,
                );            
            }else{
                $pathUrl = 'Content/images/furniture/';
                $config = array(
                    'upload_path'   => $pathUrl,
                    'allowed_types' => 'gif|jpg|png',
                    'max_size'      => '10000',
                    'max_width'     => '102400',
                    'max_height'    => '76008',
                    'encrypt_name'  => false,
                );            
            }

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                //echo "fail";
                $paramGallery = "";
            } else {
                $upload_data = $this->upload->data();
                //echo "success"."<br/>";
                $url = $upload_data['file_name'];
                $paramGallery = $pathUrl.$url;
            }
        //}

        if($_FILES['thumb']){
            $pathUrlThumb = 'Content/images/Thumbnail/';
                $config = array(
                    'upload_path'   => $pathUrlThumb,
                    'allowed_types' => 'gif|jpg|png',
                    'max_size'      => '10000',
                    'max_width'     => '102400',
                    'max_height'    => '76008',
                    'encrypt_name'  => false
                );  
            $this->upload->initialize($config); 

            if (!$this->upload->do_upload('thumb')) {
                //echo "fail";
                $paramThumb = "";
            } else {
                $upload_data = $this->upload->data();
                //echo "success"."<br/>";
                $thumb = $upload_data['file_name'];
                $paramThumb = $pathUrlThumb.$thumb;
            }
        }

        $param = array(
            'type' => $data['galleryType'],
            'name' => $data['caption'],
            'url' => $paramGallery,
            'thumbnail' => $paramThumb,
            // 'instance_name' => $data['ins_name']
        );
        // print_r($_FILES['userfile']);
        // print_r($_FILES['thumb']);


        $this->load->model('gallery_model');  
        $this->gallery_model->create($param);

        /********** Gen XML Start *************/
        $query = $this->gallery_model->getAll();

        $this->load->helper('file');
        galleryExportXml($query, 'folder');
        /********** Gen XML End *************/

        redirect('/gallery_controller', 'refresh');

    }

    public function updateData(){
        $data = $this->input->post();

        $id = $data['id'];
        $param = array(
            'type' => $data['type'],
            'name' => $data['name'],
            // 'instance_name' => $data['instance_name'] 
            );

        $this->load->model('gallery_model');  
        $this->gallery_model->update($param, $id);

        /********** Gen XML Start *************/
        $query = $this->gallery_model->getAll();

        $this->load->helper('file');
        galleryExportXml($query, 'folder');
        /********** Gen XML End *************/

    }

    public function delete(){
        $id = $this->input->post('id');

        $this->load->model('gallery_model');  
        $this->gallery_model->delete($id);

        /********** Gen XML Start *************/
        $query = $this->gallery_model->getAll();
        
        $this->load->helper('file');
        galleryExportXml($query, 'folder');
        /********** Gen XML End *************/
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */