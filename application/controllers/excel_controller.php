<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Excel_controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        if (!$this->session->userdata('email')||!$password = $this->session->userdata('password'))
        {
            $logout = base_url();
            redirect($logout);
        }
    }

    public function index() {

        $this->load->view('excel/excel');
    }

    public function do_upload() {
        $whitelist = array('xls');
        $name      = null;
        $error     = 'No file uploaded.';

        if (isset($_FILES)) {
          if (isset($_FILES['file'])) {
            $tmp_name = $_FILES['file']['tmp_name'];
            $name     = basename($_FILES['file']['name']);
            $error    = $_FILES['file']['error'];

            if ($error === UPLOAD_ERR_OK) {
              $extension = pathinfo($name, PATHINFO_EXTENSION);

              if (!in_array($extension, $whitelist)) {
                $error = 'Invalid file type uploaded.';
              } else {
                move_uploaded_file($tmp_name, $name);
              }
            }
          }
        }

        $this->excel();

        echo json_encode(array(
          'name'  => $name,
          'error' => $error,
        ));

        redirect('/excel_controller/index');
    }

    public function excel(){
        include 'excel_reader.php';     // include the class

        $this->load->library('excel');

        $this->load->database();
        $this->load->model('Room_model');

        $inputFileName = "knightsbridge_data.xls";
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($inputFileName);

        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
        $highestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();

        echo "amount:: ".$highestRow."<br/>";

        $roomtype_model = $this->Room_model->getRoomType()->result();
        $roomFunc_model = $this->Room_model->getRoomFunction()->result();

        echo "<pre/>";
        // print_r($objPHPExcel);
        // for( $i=5 ; $i<=$highestRow ; $i++ )
        // {
        //   $roomtype = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
        //   $data_insert = array(
        //     'type' => $roomtype,
        //   );
        //   echo "roomtype:: ".$roomtype."<br/>";
        //   $querys = $this->Room_model->insertTypeAll($data_insert);
        // }
        // exit();

        for( $i=5 ; $i<=$highestRow ; $i++ )
          {
              $roomcode = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
              $tower = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
              $floor = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
              $price_per_sqm = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
              $area = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
              $price = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
              $roomtype = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
              $tfs = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
              $typedef = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
              $compass = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
              $zone = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
              $mb = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
              $discount = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
              $budget_price = $objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
              $booking = $objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
              $contract = $objWorksheet->getCellByColumnAndRow(15,$i)->getValue();

              $floor_id = $this->Room_model->getFloorByTower('A', $floor)->result();

              // $floor = substr($roomcode,0,2);

              foreach ($roomtype_model as $key => $value) {
                  if($value->room_type == $roomtype){
                    $roomtype_id = $value->room_id;
                  }
              }

              foreach ($roomFunc_model as $key => $value) {
                  if($value->detail == $typedef){
                    $roomFunc_id = $value->id;
                  }
              }

              // echo "$roomtype_id"."<br/>";
              // echo "$roomFunc_id"."<br/>";
              // exit;

            $query = $this->Room_model->getRoomcode($roomcode)->result();
            $qroomcode = (isset($query[0]->room_code))?$query[0]->room_code:0;

            if($qroomcode == $roomcode){
              $data_insert = array(
                  'price_sqm' => round($price_per_sqm),
                  'size' => $area,
                  'price' => $price,
                  'room_type' => $roomtype_id,
                  'tfs' => $tfs,
                  'room_function' => $roomFunc_id,
                  'compass' => $compass,
                  'zone' => $zone,
                  'mb' => round($mb),
                  'discount' => $discount,
                  'budget_price' => $budget_price,
                  'reservation' => $booking,
                  'contract' => $contract,

              );
              // $querys = $this->Room_model->updateRoom($query[0]->id,$data_insert);
            }else{
              // $tower_id = ($tower == 'A')?1:2;
              $data_insert = array(
                  'room_code' => $roomcode,
                  'tower' => 1,
                  'floor' => $floor_id[0]->id,
                  'price_sqm' => round($price_per_sqm),
                  'size' => $area,
                  'price' => $price,
                  'room_type' => $roomtype_id,
                  'tfs' => $tfs,
                  'room_function' => $roomFunc_id,
                  'compass' => $compass,
                  'zone' => $zone,
                  'mb' => round($mb),
                  'discount' => $discount,
                  'budget_price' => $budget_price,
                  'reservation' => $booking,
                  'contract' => $contract,

                  // 'balloon' => $balloon,
                  // 'installment' => $installment,
                  // 'amount' => str_replace(",","",$amount),
                  // 'ownership' => $ownership,
                  // 'floor' => $floor,
                  'status' => 1,

              );
              print_r($data_insert);
              $querys = $this->Room_model->insertRoom($data_insert);

          }
        }
    }

    public function exportexcel(){
        // Create your database query
        $this->load->database();
        // $this->load->model('Excel_model');
        // $query = $this->Excel_model->getRoom()->result();
        $this->load->model('Room_model');
        $query = $this->Room_model->getRoom()->result();

        // echo "<pre/>";
        // print_r($query->result());
        // exit();

        include 'excel_reader.php';     // include the class

        $this->load->library('excel');

        // Instantiate a new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $sheet = $objPHPExcel->getActiveSheet();

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $styleArray = array(
              'borders' => array(
                  'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                  )
              )
          );

        // Set the active Excel worksheet to sheet 0
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->mergeCells('B2:K2');
        $sheet->getStyle("B2:K2")->applyFromArray($style);
        $objPHPExcel->getActiveSheet()->SetCellValue('B2', 'Knightsbridge Room Data');

        // $objPHPExcel->getActiveSheet()->mergeCells('I3:R3');
        //  $sheet->getStyle("I3:R3")->applyFromArray($style);
        //  $sheet->getStyle("I3:R3")->applyFromArray($styleArray);
        // $objPHPExcel->getActiveSheet()->SetCellValue('I3', 'แบบการผ่อนดาว์นแบบที่ 1');

        $sheet->getStyle("A4:P4")->applyFromArray($style);
        $sheet->getStyle("P")->applyFromArray($style);
        $sheet->getStyle("A4:P4")->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);

        $objPHPExcel->getActiveSheet()->SetCellValue('A4', 'Room NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Tower');
        $objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Floor');
        $objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Price / sq.m.');
        $objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Area');
        $objPHPExcel->getActiveSheet()->SetCellValue('F4', 'Price');
        $objPHPExcel->getActiveSheet()->SetCellValue('G4', 'Room Type');
        $objPHPExcel->getActiveSheet()->SetCellValue('H4', 'Type For Sale');
        $objPHPExcel->getActiveSheet()->SetCellValue('I4', 'Type Def');
        $objPHPExcel->getActiveSheet()->SetCellValue('J4', 'Compass');
        $objPHPExcel->getActiveSheet()->SetCellValue('K4', 'Zone');
        $objPHPExcel->getActiveSheet()->SetCellValue('L4', 'MB');
        $objPHPExcel->getActiveSheet()->SetCellValue('M4', 'Discount');
        $objPHPExcel->getActiveSheet()->SetCellValue('N4', 'Budget Price');
        $objPHPExcel->getActiveSheet()->SetCellValue('O4', 'Booking');
        $objPHPExcel->getActiveSheet()->SetCellValue('P4', 'Contract');

        // Initialise the Excel row number
        $rowCount = 5;
        // Iterate through each result from the SQL query in turn
        // We fetch each database result row into $row in turn
        foreach ($query as $result) {
            // Set cell An to the "name" column from the database (assuming you have a column called name)
            //    where n is the Excel row number (ie cell A1 in the first row)
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$rowCount, $result->room_code, PHPExcel_Cell_DataType::TYPE_STRING);
            // Set cell Bn to the "age" column from the database (assuming you have a column called age)
            //    where n is the Excel row number (ie cell A1 in the first row)
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, 'A');
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $result->floorname);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $result->price_sqm);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $result->size);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $result->price);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $result->room_type);
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $result->tfs);
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $result->detail);
            $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $result->compass);
            $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, '');
            $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $result->mb);
            $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $result->discount);
            $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $result->budget_price);
            $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, $result->reservation);
            $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, $result->contract);
            // Increment the Excel row counter
            $rowCount++;
        }

        // Instantiate a Writer to create an OfficeOpenXML Excel .xls file
        // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('knightsbridge_data.xls');
    }

    public function exportCustomer(){
        // Create your database query
        $this->load->database();
        $this->load->model('Excel_model');
        $query = $this->Excel_model->getCustomer()->result();

        // echo "<pre/>";
        // print_r($query);
        // exit();

        include 'excel_reader.php';     // include the class

        $this->load->library('excel');

        // Instantiate a new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $sheet = $objPHPExcel->getActiveSheet();

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $styleArray = array(
              'borders' => array(
                  'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                  )
              )
          );

        // Set the active Excel worksheet to sheet 0
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->mergeCells('A2:F2');
        $sheet->getStyle("A2:F2")->applyFromArray($style);
        $objPHPExcel->getActiveSheet()->SetCellValue('A2', 'Richy Customer Data');

        // $objPHPExcel->getActiveSheet()->mergeCells('I3:R3');
        //  $sheet->getStyle("I3:R3")->applyFromArray($style);
        //  $sheet->getStyle("I3:R3")->applyFromArray($styleArray);
        // $objPHPExcel->getActiveSheet()->SetCellValue('I3', 'แบบการผ่อนดาว์นแบบที่ 1');

        // $sheet->getStyle("A4:L4")->applyFromArray($style);
        // $sheet->getStyle("L")->applyFromArray($style);
        // $sheet->getStyle("A4:L4")->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);

        $objPHPExcel->getActiveSheet()->SetCellValue('A3', 'ชื่อ');
        $objPHPExcel->getActiveSheet()->SetCellValue('B3', 'นามสกุล');
        $objPHPExcel->getActiveSheet()->SetCellValue('C3', 'อีเมล์');
        $objPHPExcel->getActiveSheet()->SetCellValue('D3', 'โทร');
        $objPHPExcel->getActiveSheet()->SetCellValue('E3', 'ชั้น / ห้อง');
        $objPHPExcel->getActiveSheet()->SetCellValue('F3', 'วันที่');
        $objPHPExcel->getActiveSheet()->SetCellValue('G3', 'TEST');
        $objPHPExcel->getActiveSheet()->SetCellValue('H3', 'TEST');
        $objPHPExcel->getActiveSheet()->SetCellValue('I3', 'TEST');

        // Initialise the Excel row number
        $rowCount = 4;
        // Iterate through each result from the SQL query in turn
        // We fetch each database result row into $row in turn
        foreach ($query as $result) {
            // Set cell An to the "name" column from the database (assuming you have a column called name)
            //    where n is the Excel row number (ie cell A1 in the first row)

            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $result->firstname);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $result->lastname);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $result->email);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$rowCount, $result->tel, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$rowCount, $result->roomcode, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $result->date);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, 5);
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, 3);
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, '=SUM(G'.$rowCount.':H'.$rowCount.')');
            // Increment the Excel row counter
            $rowCount++;
        }

        // Instantiate a Writer to create an OfficeOpenXML Excel .xls file
        // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        // Write the Excel file to filename some_excel_file.xlsx in the current directory
        $objWriter->save('customer_data.xls');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
