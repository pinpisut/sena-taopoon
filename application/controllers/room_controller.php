<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Room_controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        if (!$this->session->userdata('email')||!$password = $this->session->userdata('password'))
        {
            $logout = base_url();
            redirect($logout);
        }
    }
    
    public function index() {
        $data = $this->input->post();

        $this->load->database();
        $this->load->model('Room_model');
        $query = $this->Room_model->getRoom();

        // echo '<pre/>';
        // print_r($query->result());
        // exit();

        $tower_name = $data['list_tower'];

        $list_tower = $this->Room_model->getTower()->result();
        $list_roomType = $this->Room_model->getRoomType()->result();
        $list_status = $this->Room_model->getStatus()->result();
        // $list_roomfunction = $this->Room_model->getRoomFunction()->result();
        $list_floor = $this->Room_model->getListFloor($tower_name)->result();
        $list_floor_first = $this->Room_model->getListFloorFirst()->result();

        $data['list_tower'] = json_encode($list_tower);
        $data['list_roomType'] = json_encode($list_roomType);
        $data['list_status'] = json_encode($list_status);
        // $data['list_roomfunction'] = json_encode($list_roomfunction);
        $data['list_floor'] = json_encode($list_floor);
        $data['list_floor_first'] = json_encode($list_floor_first);
        $data['results'] = $query->result();        
 
        $this->load->view('room/room',$data);
    }

    public function tower(){
        $this->load->database();
        $this->load->model('Room_model');
        $query = $this->Room_model->getTower();

        $data['results'] = $query->result();

        $this->load->view('room/tower',$data);
    }

    public function insertTower(){
        $data = $this->input->post();

        $tower_name = $data['name'];

        $this->load->database();
        $this->load->model('Room_model');

        if ($data['id'] == 0) {
            $query = $this->Room_model->insertTower($tower_name);
        }else{
            $this->Room_model->updateTower($data['id'], $tower_name);
            $query = $data['id'];
        }

        $response = array(
            'data' => $data,
            'last_id' => $query
        );
        echo json_encode($response);        
    }

    public function deleteTower(){
        $data = $this->input->post();

        $tower_id = $data['id'];

        $this->load->database();
        $this->load->model('Room_model');
        $this->Room_model->deleteTower($tower_id);

    }

    public function updateImg(){
        $this->load->model('Room_model');  

        $dataID = $this->input->post('id');
        $query = $this->Room_model->getFloorByID($dataID);      

        $row = $query->row();
        $image = $row->image;

        if($this->input->post('type') == 'img'){
            if(file_exists($image)){
                unlink($image);
            }
        }

        if($this->input->post('type') == 'img'){
            $pathUrl = 'Content/images/floor/';
            $config = array(
                'upload_path'   => $pathUrl,
                'allowed_types' => 'gif|jpg|png',
                'max_size'      => '10000',
                'max_width'     => '102400',
                'max_height'    => '76008',
                'encrypt_name'  => false,
            );      

        }else{

            $pathUrl = 'Content/images/floor/';
            $config = array(
                'upload_path'   => $pathUrl,
                'allowed_types' => 'gif|jpg|png',
                'max_size'      => '10000',
                'max_width'     => '102400',
                'max_height'    => '76008',
                'encrypt_name'  => false,
            );  
        }

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            echo "fail";
            echo $this->upload->display_errors();
        } else {
            $upload_data = $this->upload->data();
            $url = $upload_data['file_name'];
        }


        if($this->input->post('type') == 'img'){
            $param = array(
                'image' => $pathUrl.$url
            );
        }else{
            $param = array(
                'image' => $pathUrl.$url
            );
        }

        $this->Room_model->update($param, $dataID);

        /****** Gen XML File Start *******/
        $query = $this->Room_model->getRoom();

        $this->load->helper('file');
        // roomTypeExportXml($query, 'folder');
        /****** Gen XML File End *******/

        echo json_encode(array('img_path' => base_url().'/'.$pathUrl.$url));
    }

    public function floor(){

        // $this->load->helper('url');
        // $tower_id = $this->uri->segment(3);

        $this->load->database();
        $this->load->model('Room_model');
        $query = $this->Room_model->getFloorAll(); //$tower_id

        $list_tower = $this->Room_model->getTower()->result();
        //$data_tower = $this->Room_model->getTowerWhere($tower_id)->result();

        $data['results'] = $query->result();
        $data['list_tower'] = json_encode($list_tower);
        //$data['data_tower'] = json_encode($data_tower);

        $this->load->view('room/floor',$data);
    }

    public function insertFloor(){
        $data = $this->input->post();

        $tower_name = $data['tower'];
        $floor = $data['floor'];
        $id = $data['id'];

        $this->load->database();
        $this->load->model('Room_model');
        if($id == 0){
            $query = $this->Room_model->insertFloor($tower_name,$floor);
        }else{
            $this->Room_model->updateFloor($id,$floor);
            $query = $id;
        }

        $response = array(
            'data' => $data,
            'last_id' => $query
        );
        echo json_encode($response);
        
    }

    public function getListFloor(){
        $data = $this->input->post();
       // $data['aa'] = 'ddd';
      //  print_r($data); exit;
        $tower_name = $data['list_tower'];

        $this->load->database();
        $this->load->model('Room_model');
        $list_floor = $this->Room_model->getListFloor($tower_name)->result();

        $response = array(
            'data' => $data,
            'list_floor' => $list_floor
        );
        echo json_encode($response);
    }

    public function deleteFloor(){
        $data = $this->input->post();

        $floor_id = $data['id'];

        $this->load->database();
        $this->load->model('Room_model');
        $this->Room_model->deleteFloor($floor_id);

    }

    public function getTowerPageFloor(){
        $this->load->database();
        $this->load->model('Room_model');
        $query = $this->Room_model->getTower();

        $data['results'] = $query->result();

        $this->load->view('room/addfloor_show_tower',$data);
    }

    public function insertRoom(){
        $data = $this->input->post();

        $id = $data['id'];
        $room = $data['room'];
        $tower = $data['tower'];
        $floor = $data['floor'];
        $room_type = $data['room_type'];
        $area = $data['area'];
        $price = $data['price'];
        $compass = $data['compass'];
        $status = $data['status'];
        $room_no = $data['room_no'];
        $unit_no = $data['unit_no'];

        $data_insert = array(
                'room_code' => $room,
                'tower' => $tower,
                'floor' => $floor,
                'size' => $area,
                'room_type' => $room_type,
                'price' => $price,
                'compass' => $compass,
                'status' => $status,
        );

        $data_update = array(
                'size' => $area,
                'room_type' => $room_type, 
                'price' => $price,
                'compass' => $compass,
                'status' => $status,
                'room_no' => $room_no,
                'unit_no' => $unit_no,
        );
        
        $this->load->database();
        $this->load->model('Room_model');

        if($id == 0){
            $query = $this->Room_model->insertRoom($data_insert);

            //START GEN XML
            $queryRoom = $this->Room_model->getRoom();
            $queryStatus = $this->Room_model->getStatusForXML();

            $this->load->helper('file');

            roomExportXml($queryRoom, 'folder');
            roomStatusExportXml($queryStatus, 'folder');
            //END GEN XML
        }else{
            $this->Room_model->updateRoom($id, $data_update);
            $query = $id;

            //START GEN XML
            $queryRoom = $this->Room_model->getRoom();

            // $querySt = $this->Room_model->getStatus();
            // $queryStatus = $this->Room_model->getStatusForXML();
            

            $this->load->helper('file');

            roomExportJson($queryRoom, 'folder');

            // roomExportXml($queryRoom, 'folder');
            // roomStatusExportXml($queryStatus, 'folder');
            //END GEN XML
        }

        $response = array(
            'data' => $data,
            'last_id' => $query
        );
        echo json_encode($response);
        
    }

    public function deleteRoom(){
        $data = $this->input->post();

        $room_id = $data['id'];

        $this->load->database();
        $this->load->model('Room_model');
        $this->Room_model->deleteRoom($room_id);

    }

    public function testRoomXml(){
        $this->load->database();
        $this->load->model('Room_model');
        // $query = $this->Room_model->getRoom();
        $queryRoom = $this->Room_model->getRoom();

        // echo "<pre/>";
        // print_r($queryRoom->result());

        $this->load->helper('file');
        roomExportJson($queryRoom, 'folder');

        // roomExportXml($query, 'folder');
    }

    public function testStatusXML(){
        $this->load->database();
        $this->load->model('Room_model');
        $query = $this->Room_model->getStatusForXML();

        $this->load->helper('file');

        roomStatusExportXml($query, 'folder');

    }

    public function testRoomTypeXML(){
        $this->load->database();
        $this->load->model('Room_model');
        $query = $this->Room_model->getRoomTypeForXML();

        $this->load->helper('file');

        roomTypeExportXml($query, '');

    }

    public function testGalleryXML(){
        $this->load->database();
        $this->load->model('Room_model');
        $query = $this->Room_model->getGalleryForXML();

        $this->load->helper('file');

        galleryExportXml($query, '');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */