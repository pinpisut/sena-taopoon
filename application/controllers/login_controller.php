<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_Controller extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -  
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        //echo "function index!!!";
        $this->load->view('login');
    }

    public function login_page() {

        $email = $this->input->post('email');
        $password = md5($this->input->post('pwd'));

        //$mypassword = md5 ($_POST['password']);

        $this->load->database();
        $this->load->model('Login_Model');
        $query = $this->Login_Model->get_info_login($email, $password);

        // echo "<pre/>";
        // print_r($query->result());
        // exit();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $name = $row->name;
                $lastname = $row->lastname;
                $permission = $row->permission;
            }

            // echo "permission: ".$permission;
            // exit();

            // SET SESSION
            $this->load->library('session');
            $newdata = array(
                'email' => $email,
                'password' => $password,
                'name' => $name,
                'permission' => $permission,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($newdata);

            //redirect('Login_Controller/welcome');
            redirect('room_controller');

            //$this->load->view('welcome');
        } else {
            $logout = base_url();
            redirect($logout);
            //echo "ไม่พบข้อมูล";
            //$data['message'] = 'ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง';
            //$this->load->view('login',$data);
        }
    }

    public function welcome() {
        $this->load->database();
        $this->load->model('Login_Model');
        $this->load->model('Permission_model');
        $query = $this->Login_Model->get_all_info();

        $user_id = array();
        $name = array();
        $lastname = array();
        $tel = array();
        $email = array();
        $password = array();
        $permission = array();
        $num = 0;

        $permission = $this->Permission_model->all()->result();
        $data['results'] = $query->result();
//        $data['permission'] = 
        $data['permission'] = json_encode($permission);

        $this->load->view('welcome', $data);
    }

    public function insert_user() {
        //echo "5555555";
        $data = $this->input->post();

        // $name = $data['name'];
        // $lastname = $data['lastname'];
        // $tel = $data['tel'];
        // $email = $data['email'];
        // $password = md5($data['password']);
        // $permission = $data['permission'];

        

        $this->load->database();
        $this->load->model('Login_Model');        

        if ($data['id'] == 0) {    
            $param = array(
               'name' => $data['name'] ,
               'lastname' => $data['lastname'] ,
               'tel' => $data['tel'] ,
               'email' => $data['email'] ,
               'password' => md5($data['password']) ,
               'permission' => $data['permission']
            );

            $query = $this->Login_Model->insert_user($param);
        } else {
            $param = array(
               'name' => $data['name'] ,
               'lastname' => $data['lastname'] ,
               'tel' => $data['tel'] ,
               'email' => $data['email'] ,
               'permission' => $data['permission']
            );

            $this->Login_Model->update_user($param, $data['id']);
            $query = $data['id'];
        }

        $response = array(
            'data' => $data,
            'last_id' => $query
        );
        echo json_encode($response);
    }

    public function delete_user() {
        $data = $this->input->post();
        $user_id = $data['id'];
        
        $this->load->database();
        $this->load->model('Login_Model');
        $query = $this->Login_Model->delete_user($user_id);

    }

    public function logout(){
        //$this->session->unset_userdata('email');
        //$this->session->unset_userdata('password');
        //$this->session->unset_userdata($newdata);
        $logout = base_url();
        redirect($logout);
    }

    public function update_password(){
        $data = $this->input->post();

        $pass = $data['modal-pass'];
        $comfPass = $data['modal-pass-conf'];
        $id = $data['id'];

        $param = array(
            'password' => md5($pass)
            );

        $this->load->model('Login_Model');
        $this->Login_Model->update_user($param, $id);

    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */