<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quotation_controller extends CI_Controller {

    public function index(){
        
        $data = $this->input->post();

        $quotation = $data['quotation'];
        $firstname = $data['firstname'];
        $lastname = $data['lastname'];
        $email = $data['email'];
        $tel = $data['tel'];
        $roomcode = $data['roomcode'];
        $date = date("Y/m/d");

        $data_insert = array(
                'quotation' => $quotation,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'tel' => $tel,                
                'roomcode' => $roomcode,
                'date' => $date
        );
        
        $this->load->database();
        $this->load->model('Quotation_model');

        
        $query = $this->Quotation_model->insert($data_insert);    

        /********** Create Quotation Pdf File ***********/    
        $this->load->model('Quotation_model');
        $query = $this->Quotation_model->getQuotation($roomcode)->result();

        $this->load->library('Pdf');

        $pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('Ouatation');
        $pdf->SetFont('freeserif', '', 14, '', true);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
     
        $pdf->AddPage('L');
        
        $data['query'] = $query;
        array_push($data['query'], array('date' => $date, 'firstname' => $firstname, 'lastname' => $lastname, 'email' => $email, 'tel' => $tel ));
        
        $html = $this->load->view('pdf_view_api', $data, true);
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->lastPage();        

        $filename = "Content/quotation/".$quotation.".pdf";
         
        $pdf->Output( $filename, 'F' );
        
    }

    public function submitForm(){

        $data = $this->input->post();

        $quotation = $data["qno"];
        $firstname = $data['firstname'];
        $lastname = $data['lastname'];
        $email = $data['email'];
        $tel = $data['tel'];
        $roomcode = $data['roomcode'];
        $discount = $data['discount'];
        $date = date("Y/m/d");

        $data_insert = array(
                'quotation' => $quotation,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'tel' => $tel,                
                'roomcode' => $roomcode,
                'discount' => $discount,
                'date' => $date,
                'type' => 'backoffice'
        );
        
        $this->load->database();
        $this->load->model('Quotation_model');

        
        $query = $this->Quotation_model->insert($data_insert);    

        /********** Create Quotation Pdf File ***********/    
        $this->load->model('Quotation_model');
        $query = $this->Quotation_model->getQuotation($roomcode)->result();

        $this->load->library('Pdf');

        $pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('Ouatation');
        $pdf->SetFont('freeserif', '', 14, '', true);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
     
        $pdf->AddPage('L');
        
        $data['query'] = $query;
        array_push($data['query'], array('date' => $date, 'firstname' => $firstname, 'lastname' => $lastname, 'email' => $email, 'tel' => $tel, 'discount' => $discount ));
        
        $html = $this->load->view('pdf_view_api', $data, true);
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->lastPage();        

        $filename = "Content/quotation/".$quotation.".pdf";
         
        $pdf->Output( $filename, 'F' );

        header('Location: '.'http://localhost:8080/richybackoffice/index.php/Room_controller');
        
    }

}
