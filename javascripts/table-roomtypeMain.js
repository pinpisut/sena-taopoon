function restoreRow ( oTable, nRow )
{
  var aData = oTable.fnGetData(nRow);
  var jqTds = $('>td', nRow);
  
  for ( var i=0, iLen=jqTds.length ; i<iLen ; i++ ) {
    oTable.fnUpdate( aData[i], nRow, i, false );
  }
  
  oTable.fnDraw();
}

function editRow ( oTable, nRow )
{
  var aData = oTable.fnGetData(nRow);
  var jqTds = $('>td', nRow);

  var btn_img = $(nRow).find('.url_room');
  btn_img.show();

  var btn = btn_img.attr('id');
  var id = $(nRow).data('roomid');
  var w = btn_img.data('width');
  var h = btn_img.data('height');

  // var btn_temp = $(nRow).find('.url_detail');
  // btn_temp.show();
  // console.log(btn);

  // var btn2 = btn_temp.attr('id');
  // var id2 = $(nRow).data('roomid');
  // var w2 = btn_temp.data('width');
  // var h2 = btn_temp.data('height');

  uploadImages(btn, id, w, h, 'img');
  //uploadImages(btn2, id2, w2, h2, 'temp');

  
  // Start Dropdown Bedroom
  // var funcOrgValue = aData[1];
   var dropDown_func = '<select class="form-control pull-left" id="bed_room" name="bed_room" style="width:140px; margin-right:8px;">';
        // $.each(list_func, function(index, val){
                dropDown_func += '<option value="1 BEDROOM" >'+"1 BEDROOM"+'</option>';
           // });
        dropDown_func += '</select>';
  // End Drowdown Bedroom

  // Start Dropdown Bathroom
  // var funcOrgBathValue = aData[2];
   var dropDown_bath = '<select class="form-control pull-left" id="bath_room" name="bath_room" style="width:140px; margin-right:8px;">';
        // $.each(list_func, function(index, val){
                dropDown_bath += '<option value="1 BATHROOM" >'+"1 BATHROOM"+'</option>';
           // });
        dropDown_bath += '</select>';
  // End Drowdown Bathroom

  jqTds[0].innerHTML = '<input type="text" id="room_type" value="'+aData[0]+'">';
  jqTds[1].innerHTML = '<input type="text" id="size" value="'+aData[1]+'">';
  jqTds[2].innerHTML = dropDown_func;//'<input type="text" id="bed_room" value="'+aData[1]+'">';
  
  jqTds[3].innerHTML = dropDown_bath;
  //'<input type="text" id="detail" value="'+aData[3]+'">';
  // jqTds[4].innerHTML = '<input type="text" id="amount" value="'+aData[4]+'">';
  // jqTds[5].innerHTML = '<input type="text" id="startprice" value="'+aData[5]+'">';
  //jqTds[6].innerHTML = '<input type="text" id="direction" value="'+aData[6]+'">';

  
  jqTds[5].innerHTML = '<a class="edit-row" href="javascript:void(0)">Save</a>';
}

function saveRow ( oTable, nRow )
{
  var _room_type = $(nRow).find('#room_type');
  // var _roomfunction = $(nRow).find('#bed_room');
  var _size = $(nRow).find('#size');
  // var _detail = $(nRow).find('#detail');
  // var _amount = $(nRow).find('#amount');
  // var _startprice = $(nRow).find('#startprice');
  //var _direction = $(nRow).find('#direction');
  // var list_func_val = '';

  var btn_temp = $(nRow).find('.url_room');
  btn_temp.hide();
  // var btn_img = $(nRow).find('.url_detail');
  // btn_img.hide();
  var bed = document.getElementById("bed_room");
  var bedroom_value = bed.options[bed.selectedIndex].value;
  var bath = document.getElementById("bath_room");
  var bathroom_value = bath.options[bath.selectedIndex].value;

  console.log("bathroom_value: ", bathroom_value);

  // $.each(list_func, function(index, val){
  //     if(_roomfunction.val() == val.id){
  //         list_func_val = val.detail;
  //     }        
  // });
  
  oTable.fnUpdate( _room_type.val(), nRow, 0, false );
  oTable.fnUpdate( _size.val(), nRow, 1, false );
  oTable.fnUpdate( bedroom_value, nRow, 2, false );
  oTable.fnUpdate( bathroom_value, nRow, 3, false );
  // oTable.fnUpdate( _amount.val(), nRow, 4, false );
  // oTable.fnUpdate( _startprice.val(), nRow, 5, false );
  //oTable.fnUpdate( _direction.val(), nRow, 6, false );
  
  oTable.fnUpdate( '<a class="edit-row" href="javascript:void(0)">Edit</a>', nRow, 5, false );
  oTable.fnDraw();

  var id = $(nRow).data('roomid');
  $.ajax({
        url: site_url+"/roomtypemain_controller/updateData",
        data: {
            id: id,
            name: _room_type.val(),
            size: _size.val(),
            bedroom: bedroom_value,
            bathroom: bathroom_value
        },
        type: 'post',
        dataType: 'json'
    });
}

function uploadImages(btn, id, w, h, type){
    new AjaxUpload(btn, {
            action: url,
            name: 'userfile',
            data: {
                'id' : id,
                'width' : w,
                'height' : h,
                'type' : type
            },
            responseType: 'json',
            onSubmit: function(file, ext) {
                if (! (ext && /^(jpg|JPG|png|PNG|jpeg|JPEG|gif|GIF)$/.test(ext))){
                    // extension is not allowed
                    alert('Only JPG, PNG or GIF files are allowed');
                    return false;
                }
            },
            onComplete: function(file, response) {
                $('#'+btn).parent().find('img').attr('src',response.img_path);
            
                // this.setData({
                //     'id' : response.image_id,
                //     'order' : response.order,
                //     'image_name' : response.image,
                //     'category' : '<?php echo $category; ?>',
                //     'content_id' : '<?php echo $content_id; ?>',
                //     'content_type' : '<?php echo $content_type; ?>'
                // });
            }
        });
}

$(document).ready(function() {
  var oTable = $("#datatable-editable-roomtype").dataTable({
    "sPaginationType": "full_numbers",
    aoColumnDefs: [
      {
        bSortable: false,
        aTargets: [-2, -1]
      }
    ]
  });
  var nEditing = null;

  $('#add-row').click( function (e) {
    e.preventDefault();

    var aiNew = oTable.fnAddData( [ '', '', '', '', '','',
      '<a class="edit-row" href="javascript:void(0)">Edit</a>', '<a class="delete-row" href="javascript:void(0)">Delete</a>' ] );
    var nRow = oTable.fnGetNodes( aiNew[0] );
    editRow( oTable, nRow );
    nEditing = nRow;
  } );

  // delete data from db
    var _nRow = 0;
    $('#remove').click(function (e) {
        e.preventDefault();
        var id = $(_nRow).data('roomid');

        $.ajax({
            url: site_url+"/roomtypemain_controller/delete",
            data: {
                id: id
            },
            type: 'post',
            dataType: 'html'
        }).done(function (data) {
            oTable.fnDeleteRow(_nRow);

            $('#confirm-delete').modal('hide');
            console.log('Deleted!!');

        });
        console.log('id: ',id);
    });

  $('#datatable-editable-roomtype').on('click', 'a.delete-row', function (e) {
    e.preventDefault();

    var nRow = $(this).parents('tr')[0];
    _nRow = nRow;
    //oTable.fnDeleteRow( nRow );

    $('#confirm-delete').modal('show');
  } );

  $('#datatable-editable-roomtype').on('click', 'a.edit-row', function (e) {
    e.preventDefault();

    /* Get the row as a parent of the link that was clicked on */
    var nRow = $(this).parents('tr')[0];

    if ( nEditing !== null && nEditing != nRow ) {
      /* Currently editing - but not this row - restore the old before continuing to edit mode */
      restoreRow( oTable, nEditing );
      editRow( oTable, nRow );
      nEditing = nRow;
    }
    else if ( nEditing == nRow && this.innerHTML == "Save" ) {
      /* Editing this row and want to save it */
      saveRow( oTable, nEditing );
      nEditing = null;
    }
    else {
      /* No edit in progress - let's start one */
      editRow( oTable, nRow );
      nEditing = nRow;
    }
  } );
} );
