var jqInputs = new Array(6);
var addValue = '';

function restoreRow ( oTable, nRow )
{
  var aData = oTable.fnGetData(nRow);
  var jqTds = $('>td', nRow);
  
  for ( var i=0, iLen=jqTds.length ; i<iLen ; i++ ) {
    oTable.fnUpdate( aData[i], nRow, i, false );
  }
  
  oTable.fnDraw();
}

function editRow ( oTable, nRow )
{
  var aData = oTable.fnGetData(nRow);
  var jqTds = $('>td', nRow);
  if(addValue == 'ADD'){
      jqTds[4].innerHTML = '<input type="password" value="'+aData[4]+'">';
  }else{
      var btn = $(nRow).find('.pass-user');
      var btnid = btn.attr('id');
      document.getElementById(btnid).disabled = false;
  }

  var permOrgValue = aData[5];
  var dropDown = '<select class="form-control pull-left" id="permission" name="permission" style="width:150px; margin-right:8px;">';
  $.each(permission, function(index, val){
    //dropDown += '<option value="'+val.permission_id+'">'+val.type+'</option>';
    dropDown += '<option value="'+val.permission_id+'" '+ ((val.type == permOrgValue) ? 'selected' : '') +'>'+val.type+'</option>';
  });
    dropDown += '</select>';

  
  //var dropdown_menu = '<select id="permis"><option value="Admin">Admin</option></select>';
  jqTds[0].innerHTML = '<input type="text" value="'+aData[0]+'">';
  jqTds[1].innerHTML = '<input type="text" value="'+aData[1]+'">';
  jqTds[2].innerHTML = '<input type="text" value="'+aData[2]+'">';
  jqTds[3].innerHTML = '<input type="text" value="'+aData[3]+'">';

  // if (aData[4] == '' || aData[4] == null) {
      
  // }
  //jqTds[4].innerHTML = '<input type="password" value="'+aData[4]+'">';
  //jqTds[5].innerHTML = '<input type="text" value="'+aData[5]+'">';
  jqTds[5].innerHTML = dropDown;

  jqTds[6].innerHTML = '<a class="edit-row" href="javascript:void(0)">Save</a>';
  //console.log('edit row2');

  //console.log('aData',aData[0]);
}

function saveRow ( oTable, nRow ) //case add user
{
  jqInputs = $('input', nRow);
  var _permission = $(nRow).find('#permission');
  console.log('permission : ',_permission.val());
  var pm = "";

  if(_permission.val() == 1){
      pm = "Super Admin";
  }else if(_permission.val() == 2){
      pm = "Admin";
  }else{
      pm = "User";
  }
  //console.log("dfrfr:: ", jqInputs[4].value);
  console.log("paramTest:: ", addValue);

  if(addValue == 'ADD'){
      oTable.fnUpdate( jqInputs[4].value, nRow, 4, false );
  }else{
      var btn = $(nRow).find('.pass-user');
      var btnid = btn.attr('id');
      document.getElementById(btnid).disabled = true;
  }

  oTable.fnUpdate( jqInputs[0].value, nRow, 0, false );
  oTable.fnUpdate( jqInputs[1].value, nRow, 1, false );
  oTable.fnUpdate( jqInputs[2].value, nRow, 2, false );
  oTable.fnUpdate( jqInputs[3].value, nRow, 3, false );
  //oTable.fnUpdate( jqInputs[4].value, nRow, 4, false );
  //oTable.fnUpdate( _permission.val(), nRow, 5, false );
  oTable.fnUpdate( pm, nRow, 5, false);
  oTable.fnUpdate( '<a class="edit-row" href="javascript:void(0)">Edit</a>', nRow, 6, false );
  oTable.fnDraw();
  var id = $(nRow).data('id');
  if(!$(nRow).data('id')){
    id = 0;
  }

  if(addValue == 'ADD'){
      $.ajax( {
        url : "insert_user",
        data: {
          name: jqInputs[0].value,
          lastname: jqInputs[1].value,
          tel: jqInputs[2].value,
          email: jqInputs[3].value,
          password: jqInputs[4].value,
          permission: _permission.val(),
          id : id
        },
        type:'post',
        dataType : 'json'
      }).done(function(data){
        console.log('55555555555');
        if(id == 0){
          $(nRow).data('id', data.last_id);
        }
      });
  }else{
      $.ajax( {
        url : "insert_user",
        data: {
          name: jqInputs[0].value,
          lastname: jqInputs[1].value,
          tel: jqInputs[2].value,
          email: jqInputs[3].value,
          //password: jqInputs[4].value,
          permission: _permission.val(),
          id : id
        },
        type:'post',
        dataType : 'json'
      }).done(function(data){
        console.log('55555555555');
        if(id == 0){
          $(nRow).data('id', data.last_id);
        }
      });
  }
      
  //console.log('aData',jqInputs[0].value);
  addValue = '';
}

$(document).ready(function() {
  var oTable = $("#datatable-editable").dataTable({
    "sPaginationType": "full_numbers",
    aoColumnDefs: [
      {
        bSortable: false,
        aTargets: [-2, -1]
      }
    ]
  });
  var nEditing = null;

  $('#add-row').click( function (e) {
    e.preventDefault();
    addValue = 'ADD';

    var aiNew = oTable.fnAddData( [ '', '', '', '', '','',
      '<a class="edit-row" href="javascript:void(0)">Edit</a>', '<a class="delete-row" href="javascript:void(0)">Delete</a>' ] );
    var nRow = oTable.fnGetNodes( aiNew[0] );
    editRow( oTable, nRow );
    nEditing = nRow;

    console.log("in add-row:: ", addValue);
  } );



var _nRow = 0; 
  // $('#datatable-editable').on('click', 'a.delete-row', function (e) {
  // } );


    $('#remove').click(function(e){
    e.preventDefault();
      var product_type_id = $(_nRow).data('id');
      
      $.ajax( {
          url : "delete_user",
          data: {
            id : product_type_id
          },
          type:'post',
          dataType : 'html'
        }).done(function(data){
            console.log('delete complete!!');
        
          oTable.fnDeleteRow( _nRow );

              $('#confirm-delete').modal('hide');
              console.log('Deleted!!');

      });
      console.log(product_type_id);
  });

    $('#save-password').click(function(e){
        e.preventDefault();

        var id = $(_nRow).data('id');
        // // var pass = 
        var frm = $('#update-password-form');
        var passdata = frm.serialize()+'&id='+id;
        console.log(frm.serialize()+'&id='+id);
        $.ajax( {
            url : "update_password",
            data: passdata,
            type:'post',
            dataType : 'html'
          }).done(function(data){
              console.log(data);
              $('#myModal').modal('hide');
        });
        // console.log("id:: ", id);
    });


  $('#datatable-editable').on('click', 'a.delete-row', function (e) {
    e.preventDefault();
    console.log('test');

    var nRow = $(this).parents('tr')[0];
    _nRow = nRow;


    var id = $(nRow).data('id');
    $('#confirm-delete').modal('show');

  });


   /* 

    var nRow = $(this).parents('tr')[0];
    var id = $(nRow).data('id');
    console.log('delete!!!!!!',id);

    $('#myModal').modal('show');

    $('.conf').click(function(e){
        console.log('delete complete!!');  
        oTable.fnDeleteRow( nRow );   

        $.ajax( {
          url : "delete_user",
          data: {
            id : id
          },
          type:'post',
          dataType : 'json'
        }).done(function(data){
            console.log('delete complete!!');
        });  
    });*/

  /*  var nRow = $(this).parents('tr')[0];
    oTable.fnDeleteRow( nRow );

    var id = $(nRow).data('id');
    console.log('delete!!!!!!',id);*/

   /* $.ajax( {
        url : "delete_user",
        data: {
          id : id
        },
        type:'post',
        dataType : 'json'
      }).done(function(data){
          console.log('delete complete!!');
        });
*/
    

  $('#datatable-editable').on('click', 'a.edit-row', function (e) {
    e.preventDefault();

    /* Get the row as a parent of the link that was clicked on */
    var nRow = $(this).parents('tr')[0];
    _nRow = nRow;

    if ( nEditing !== null && nEditing != nRow ) {
      /* Currently editing - but not this row - restore the old before continuing to edit mode */
      restoreRow( oTable, nEditing );
      editRow( oTable, nRow );
      nEditing = nRow;
      //console.log('edit row4');
    }
    else if ( nEditing == nRow && this.innerHTML == "Save" ) {
      /* Editing this row and want to save it */
      //$(nEditing).data('permission',5);
      //console.log('nEditing: ',nEditing);
      console.log('Save na');
     // $( "td.hidden-xs" ).html("Admin");
      saveRow( oTable, nEditing );
      nEditing = null;
     // console.log('edit row5');
      //console.log('name: ',jqInputs[0].value);

      
    }
    else {
      /* No edit in progress - let's start one */
      editRow( oTable, nRow );
      nEditing = nRow;
     // console.log('edit row6');
    }
  } );

} );
