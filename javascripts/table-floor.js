var jqInputs = new Array(3);

function restoreRow(oTable, nRow)
{
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);

    for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
        oTable.fnUpdate(aData[i], nRow, i, false);
    }

    oTable.fnDraw();
}

function editRow(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);

    var btn_img = $(nRow).find('.url_room');
    btn_img.show();

    var btn = btn_img.attr('id');
    var id = $(nRow).data('id');
    var w = btn_img.data('width');
    var h = btn_img.data('height');

    uploadImages(btn, id, w, h, 'img');

    // Start Dropdown Room Type
       var towerOrgValue = aData[0];
       var dropDown_tower = '<select class="form-control pull-left" id="list_tower" name="list_tower" style="width:100%; margin-right:8px;">';

               $.each(list_tower, function(index, val){
                    dropDown_tower += '<option value="'+val.id+'" '+ ((val.tower == towerOrgValue) ? 'selected' : '') +'>'+val.tower+'</option>';
               });
            dropDown_tower += '</select>';
    // End Drowdown Room Type


    jqTds[0].innerHTML = dropDown_tower;
    jqTds[1].innerHTML = '<input type="text" value="'+aData[1]+'">';
    jqTds[3].innerHTML = '<a class="edit-row" href="javascript:void(0)">Save</a>';
}

function saveRow(oTable, nRow)
{
    var _tower = $(nRow).find('#list_tower');
    var id = $(nRow).data('id');
    var _towername = '';
    console.log('_tower: '+_tower.val());
    console.log('id: '+id);

    var btn_temp = $(nRow).find('.url_room');
    btn_temp.hide();

    var jqInputs = $('input', nRow);

    $.each(list_tower, function(index, val){
        if(_tower.val() == val.id){
            _towername = val.tower;
        }        
    });

    /***** SAVE & EDIT FLOOR *****/ 
    
    if (!$(nRow).data('id')) {
        id = 0;
    }
    $.ajax({
        url: site_url+"/room_controller/insertFloor",
        data: {
            tower: _tower.val(),
            floor: jqInputs[0].value,
            id: id
        },
        type: 'post',
        dataType: 'json'
    }).done(function (data) {
        if (id == 0) {
            $(nRow).data('id', data.last_id);
        }
    });
    /***** SAVE & EDIT FLOOR END *****/ 

    oTable.fnUpdate( _towername, nRow, 0, false );
    oTable.fnUpdate( jqInputs[0].value, nRow, 1, false );
    oTable.fnUpdate( '<a class="edit-row" href="javascript:void(0)">Edit</a>', nRow, 3, false );
    oTable.fnDraw();
}

function uploadImages(btn, id, w, h, type){
    new AjaxUpload(btn, {
            action: url,
            name: 'userfile',
            data: {
                'id' : id,
                'width' : w,
                'height' : h,
                'type' : type
            },
            responseType: 'json',
            onSubmit: function(file, ext) {
                if (! (ext && /^(jpg|JPG|png|PNG|jpeg|JPEG|gif|GIF)$/.test(ext))){
                    // extension is not allowed
                    alert('Only JPG, PNG or GIF files are allowed');
                    return false;
                }
            },
            onComplete: function(file, response) {
                $('#'+btn).parent().find('img').attr('src',response.img_path);
            
                // this.setData({
                //     'id' : response.image_id,
                //     'order' : response.order,
                //     'image_name' : response.image,
                //     'category' : '<?php echo $category; ?>',
                //     'content_id' : '<?php echo $content_id; ?>',
                //     'content_type' : '<?php echo $content_type; ?>'
                // });
            }
        });
}

$(document).ready(function () {
    var oTable = $("#datatable-editable-floor").dataTable({
        "sPaginationType": "full_numbers",
        aoColumnDefs: [
            {
                bSortable: false,
                aTargets: [-2, -1]
            }
        ]
    });
    var nEditing = null;

    $('#add-row').click(function (e) {
        console.log('1');
        e.preventDefault();

        var aiNew = oTable.fnAddData(['', '', '',
            '<a class="edit-row" href="javascript:void(0)">Edit</a>', '<a class="delete-row" href="javascript:void(0)">Delete</a>']);
        var nRow = oTable.fnGetNodes(aiNew[0]);
        editRow(oTable, nRow);
        nEditing = nRow;
    });

    // delete data from db
    var _nRow = 0;
    $('#remove').click(function (e) {
        e.preventDefault();
        var floor_id = $(_nRow).data('id');

        $.ajax({
            url: site_url+"/room_controller/deleteFloor",
            data: {
                id: floor_id
            },
            type: 'post',
            dataType: 'html'
        }).done(function (data) {
            console.log('delete complete!!');

            oTable.fnDeleteRow(_nRow);

            $('#confirm-delete').modal('hide');
            console.log('Deleted!!');

        });
        console.log(floor_id);
    });

    $('#datatable-editable-floor').on('click', 'a.delete-row', function (e) {
        e.preventDefault();

        var nRow = $(this).parents('tr')[0];
        _nRow = nRow;
        //oTable.fnDeleteRow( nRow );

        var id = $(nRow).data('id');
        $('#confirm-delete').modal('show');

    });

    $('#datatable-editable-floor').on('click', 'a.edit-row', function (e) {
        e.preventDefault();

        /* Get the row as a parent of the link that was clicked on */
        var nRow = $(this).parents('tr')[0];

        if (nEditing !== null && nEditing != nRow) {
            /* Currently editing - but not this row - restore the old before continuing to edit mode */
            restoreRow(oTable, nEditing);
            editRow(oTable, nRow);
            nEditing = nRow;
        }
        else if (nEditing == nRow && this.innerHTML == "Save") {
            /* Editing this row and want to save it */
            saveRow(oTable, nEditing);
            nEditing = null;
        }
        else {
            /* No edit in progress - let's start one */
            editRow(oTable, nRow);
            nEditing = nRow;
        }
    });
});
