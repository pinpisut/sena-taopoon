function restoreRow ( oTable, nRow )
{
  var aData = oTable.fnGetData(nRow);
  var jqTds = $('>td', nRow);
  
  for ( var i=0, iLen=jqTds.length ; i<iLen ; i++ ) {
    oTable.fnUpdate( aData[i], nRow, i, false );
  }
  
  oTable.fnDraw();
}



$( "table" ).on('change','#list_tower',function() {
  var _this = $(this);
      $.ajax({
          type: 'POST',
          dataType : 'json',
          data: {
                  list_tower: $("#list_tower").val()
                  /*$(this).val()*/
                },
          url: 'room_controller/getListFloor'
      }).done(function(data){
        console.log(data);
        list_floor = data.list_floor;
        console.log('ttt : ',list_floor);
         var dropDownFloor = '<select class="form-control pull-left" id="list_floor" name="list_floor" style="width:64px; margin-right:8px;">';

            $.each(list_floor, function(index, val){
              dropDownFloor += '<option value="'+val.floor+'">'+val.floor+'</option>';
            });

          dropDownFloor += '</select>';
          console.log(_this.closest('tr').find('td').eq(2));
           _this.closest('tr').find('td').eq(2).html(dropDownFloor);
      });
      return false;
  });




function editRow ( oTable, nRow )
{
  var aData = oTable.fnGetData(nRow);

  // Start Dropdown Tower
   var dropDown_tower = '<select class="form-control pull-left" id="list_tower" name="list_tower" style="width:64px; margin-right:8px;">';

   $.each(list_tower, function(index, val){
        dropDown_tower += '<option value="'+val.id+'">'+val.tower+'</option>';
   });
        dropDown_tower += '</select>';
  // End Drowdown Tower

  // Start Dropdown Room Type
   var roomTypeOrgValue = aData[4];
   var dropDown_roomType = '<select class="form-control pull-left" id="room_type" name="room_type" style="width:79px; margin-right:8px;">';
            dropDown_roomType += '<option value="0">-- Please select --</option>'

           $.each(list_roomType, function(index, val){
                dropDown_roomType += '<option value="'+val.room_id+'" '+ ((val.room_type == roomTypeOrgValue) ? 'selected' : '') +'>'+val.room_type+'</option>';
           });
        dropDown_roomType += '</select>';
  // End Drowdown Room Type

  // Start Dropdown Compass
  var roomCompassOrgValue = aData[7];
  var dropDown_compass = '<select class="form-control pull-left" id="compass" name="compass" style="width:79px; margin-right:8px;">';
           dropDown_compass += '<option value="0">-- Please select --</option>'
           dropDown_compass += '<option value="north" '+ (('north' == roomCompassOrgValue) ? 'selected' : '') +'>'+'north'+'</option>';
           dropDown_compass += '<option value="south" '+ (('south' == roomCompassOrgValue) ? 'selected' : '') +'>'+'south'+'</option>';
       dropDown_compass += '</select>';
 // End Drowdown Compass

  
  // Start Dropdown Status
    var statusOrgValue = aData[8];
    var dropDown_status = '<select class="form-control pull-left" id="status" name="status" style="width:95px; margin-right:8px;">';

             $.each(list_status, function(index, val){
                  dropDown_status += '<option value="'+val.status_id+'"  '+ ((val.status == statusOrgValue) ? 'selected' : '') +' >'+val.status+'</option>';
             });
      dropDown_status += '</select>';
  // End Drowdown Status

  var floorOrgValue = aData[1];
  var dropDownFloor = '<select class="form-control pull-left" id="list_floor" name="list_floor" style="width:64px; margin-right:8px;">';
          $.each(list_floor_first, function(index, val){
                dropDownFloor += '<option value="'+val.floor+'" '+ ((val.floor == floorOrgValue) ? 'selected' : '') +'>'+val.floor+'</option>';
           });
     dropDownFloor += '</select>';

 
  var jqTds = $('>td', nRow);
  console.log('aData: ',aData);

  jqTds[2].innerHTML = '<input type="text" size="13" id="roomno" value="'+aData[2]+'">';
  jqTds[3].innerHTML = '<input type="text" size="13" id="unitno" value="'+aData[3]+'">';
  jqTds[4].innerHTML = dropDown_roomType;
  jqTds[5].innerHTML = '<input type="text" size="13" id="area" value="'+aData[5]+'">';
  jqTds[6].innerHTML = '<input type="text" size="13" id="price" value="'+aData[6]+'">';
  jqTds[7].innerHTML = dropDown_compass;
//   jqTds[7].innerHTML = '<input type="text" size="13" id="compass" value="'+aData[7]+'">';
  jqTds[8].innerHTML = dropDown_status;
  jqTds[9].innerHTML = '<a class="edit-row" href="javascript:void(0)" style="color:red">Save</a>';

  if (aData[0] == '' || aData[0] == null) {

      jqTds[0].innerHTML = '<input type="text" size="10" id="room" value="'+aData[0]+'">';
      jqTds[1].innerHTML = dropDownFloor;
  }
  
}

function saveRow ( oTable, nRow )
{
  var _listtower = $(nRow).find('#list_tower');
  var _roomtype = $(nRow).find('#room_type');
  var _status = $(nRow).find('#status');
  var _listfloor = $(nRow).find('#list_floor');
  var test = 'TEST';
  var list_tower_val = '';
  var list_roomtype_val = '';
  var list_status_val = '';
  

  var jqInputs = $('input', nRow);
  var jqSelect = $('select', nRow);

  $.each(list_tower, function(index, val){
      if(_listtower.val() == val.id){
          list_tower_val = val.tower;
      }        
  });

  $.each(list_roomType, function(index, val){
      if(_roomtype.val() == val.room_id){
          list_roomtype_val = val.room_type;
      }        
  });

  $.each(list_status, function(index, val){
      if(_status.val() == val.status_id){
          list_status_val = val.status;
      }        
  });

  console.log('room : ',$(nRow).find('#room').val());
  console.log('_list_tower : ',_listtower.val());
  console.log('_listfloor', _listfloor.val());
  console.log('_roomtype', _roomtype.val());
  console.log('area', $(nRow).find('#area').val());
  console.log('price', $(nRow).find('#price').val());
  console.log('compass', $(nRow).find('#compass').val());
  console.log('_status', _status.val());

  var _room = $(nRow).find('#room');
  var _area = $(nRow).find('#area');
  var _price = $(nRow).find('#price');
  var _compass = $(nRow).find('#compass');
  var _roomno = $(nRow).find('#roomno');
  var _unitno = $(nRow).find('#unitno');

  if(_room.val()){
    oTable.fnUpdate( _room.val(), nRow, 0, false );
  }
  if(_listfloor.val()){
    oTable.fnUpdate( _listfloor.val(), nRow, 1, false );
  }
  oTable.fnUpdate( _roomno.val(), nRow, 2, false );
  oTable.fnUpdate( _unitno.val(), nRow, 3, false );
  oTable.fnUpdate( list_roomtype_val, nRow, 4, false );
  oTable.fnUpdate( _area.val(), nRow, 5, false );
  oTable.fnUpdate( _price.val(), nRow, 6, false );
  oTable.fnUpdate( _compass.val(), nRow, 7, false );

  oTable.fnUpdate( list_status_val, nRow, 8, false );
  oTable.fnUpdate( '<a class="edit-row" href="javascript:void(0)">Edit</a>', nRow, 9, false );
  oTable.fnDraw();

  console.log('id: ', $(nRow).data('roomid'));

    var id = $(nRow).data('roomid');
    if (!$(nRow).data('roomid')) {
        id = 0;
    }
  console.log('id2: ',id);
    
    $.ajax({
        url: site_url+"/room_controller/insertRoom",
        data: {
            id: id,
            room: _room.val(),
            tower: _listtower.val(),
            floor: _listfloor.val(),
            room_type: _roomtype.val(),
            area: _area.val(),
            price: _price.val(),
            compass: _compass.val(),
            status: _status.val(),
            room_no: _roomno.val(),
            unit_no: _unitno.val(),
        },
        type: 'post',
        dataType: 'json'
    }).done(function (data) {
        if (id == 0) {
            $(nRow).data('roomid', data.last_id);
        }
    });
}

$(document).ready(function() {


  var oTable = $("#datatable-editable-room").dataTable({
    "scrollX": true,
    "sPaginationType": "full_numbers",
    aoColumnDefs: [
      {
        bSortable: false,
        aTargets: [-2, -1]
      }
    ]
  });
  var nEditing = null;

  $('#add-row').click( function (e) {
    e.preventDefault();

    var aiNew = oTable.fnAddData( [ '', '', '', '', '','','','', '', '','',
      '<a class="edit-row" href="javascript:void(0)">Edit</a>', '<a class="delete-row" href="javascript:void(0)">Delete</a>' ] );
    var nRow = oTable.fnGetNodes( aiNew[0] );
    editRow( oTable, nRow );
    nEditing = nRow;
  } );


  // delete data from db
    var _nRow = 0;
    $('#remove').click(function (e) {
        e.preventDefault();
        var roomid = $(_nRow).data('roomid');

        $.ajax({
            url: site_url+"/room_controller/deleteRoom",
            data: {
                id: roomid
            },
            type: 'post',
            dataType: 'html'
        }).done(function (data) {
            oTable.fnDeleteRow(_nRow);

            $('#confirm-delete').modal('hide');
            console.log('Deleted!!');

        });
        console.log('roomid: ',roomid);
    });

  $('#datatable-editable-room').on('click', 'a.delete-row', function (e) {
    e.preventDefault();

    var nRow = $(this).parents('tr')[0];
    _nRow = nRow;
    //oTable.fnDeleteRow( nRow );

    $('#confirm-delete').modal('show');
  } );
  

  $('#datatable-editable-room').on('click', 'a.edit-row', function (e) {
    e.preventDefault();
    /* Get the row as a parent of the link that was clicked on */
    var nRow = $(this).parents('tr')[0];

    if ( nEditing !== null && nEditing != nRow ) {
      /* Currently editing - but not this row - restore the old before continuing to edit mode */
      restoreRow( oTable, nEditing );
      editRow( oTable, nRow );
      nEditing = nRow;
    }
    else if ( nEditing == nRow && this.innerHTML == "Save" ) {
      /* Editing this row and want to save it */
      saveRow( oTable, nEditing );
      nEditing = null;
    }
    else {
      /* No edit in progress - let's start one */
      editRow( oTable, nRow );
      nEditing = nRow;
    }
  } );
} );
