function restoreRow ( oTable, nRow )
{
  var aData = oTable.fnGetData(nRow);
  var jqTds = $('>td', nRow);
  
  for ( var i=0, iLen=jqTds.length ; i<iLen ; i++ ) {
    oTable.fnUpdate( aData[i], nRow, i, false );
  }
  
  oTable.fnDraw();
}

function editRow ( oTable, nRow )
{
  var aData = oTable.fnGetData(nRow);
  var jqTds = $('>td', nRow);

  var btn_img = $(nRow).find('.url_room');
  btn_img.show();

  var btn = btn_img.attr('id');
  var id = $(nRow).data('roomid');
  var w = btn_img.data('width');
  var h = btn_img.data('height');

  // var btn_temp = $(nRow).find('.url_detail');
  // btn_temp.show();
  // console.log(btn);

  // var btn2 = btn_temp.attr('id');
  // var id2 = $(nRow).data('roomid');
  // var w2 = btn_temp.data('width');
  // var h2 = btn_temp.data('height');

  uploadImages(btn, id, w, h, 'img');
  //uploadImages(btn2, id2, w2, h2, 'temp');

  
  // Start Dropdown Function
  var funcOrgValue = aData[3];
   var dropDown_func = '<select class="form-control pull-left" id="room_function" name="room_function" style="width:140px; margin-right:8px;">';
        $.each(list_func, function(index, val){
                dropDown_func += '<option value="'+val.id+'" '+ ((val.name == funcOrgValue) ? 'selected' : '') +'>'+val.name+'</option>';
           });
        dropDown_func += '</select>';
  // End Drowdown Function

  jqTds[0].innerHTML = '<input type="text" id="room_type" value="'+aData[0]+'">';
  jqTds[1].innerHTML = '<input type="text" id="inprogram" value="'+aData[1]+'">';
  jqTds[2].innerHTML = '<input type="text" id="size" value="'+aData[2]+'">';
  jqTds[3].innerHTML = dropDown_func;
  
  jqTds[5].innerHTML = '<a class="edit-row" href="javascript:void(0)">Save</a>';
}

function saveRow ( oTable, nRow )
{
  var _room_type = $(nRow).find('#room_type');
  var _roomfunction = $(nRow).find('#room_function');
  var _size = $(nRow).find('#size');
  var _detail = $(nRow).find('#inprogram');

  var list_func_val = '';

  var btn_temp = $(nRow).find('.url_room');
  btn_temp.hide();
  // var btn_img = $(nRow).find('.url_detail');
  // btn_img.hide();

  $.each(list_func, function(index, val){
      if(_roomfunction.val() == val.id){
          list_func_val = val.name;
      }        
  });
  
  oTable.fnUpdate( _room_type.val(), nRow, 0, false );
  oTable.fnUpdate( _detail.val(), nRow, 1, false );
  oTable.fnUpdate( _size.val(), nRow, 2, false );
  oTable.fnUpdate( list_func_val, nRow, 3, false );
  
  oTable.fnUpdate( '<a class="edit-row" href="javascript:void(0)">Edit</a>', nRow, 5, false );
  oTable.fnDraw();

  var id = $(nRow).data('roomid');
  $.ajax({
        url: site_url+"/roomtype_controller/updateData",
        data: {
            id: id,
            room_type: _room_type.val(),
            room_function: _detail.val(),
            size: _size.val(),
            typemain: _roomfunction.val()
        },
        type: 'post',
        dataType: 'json'
    });
}

function uploadImages(btn, id, w, h, type){
    new AjaxUpload(btn, {
            action: url,
            name: 'userfile',
            data: {
                'id' : id,
                'width' : w,
                'height' : h,
                'type' : type
            },
            responseType: 'json',
            onSubmit: function(file, ext) {
                if (! (ext && /^(jpg|JPG|png|PNG|jpeg|JPEG|gif|GIF)$/.test(ext))){
                    // extension is not allowed
                    alert('Only JPG, PNG or GIF files are allowed');
                    return false;
                }
            },
            onComplete: function(file, response) {
                $('#'+btn).parent().find('img').attr('src',response.img_path);
            
                // this.setData({
                //     'id' : response.image_id,
                //     'order' : response.order,
                //     'image_name' : response.image,
                //     'category' : '<?php echo $category; ?>',
                //     'content_id' : '<?php echo $content_id; ?>',
                //     'content_type' : '<?php echo $content_type; ?>'
                // });
            }
        });
}

$(document).ready(function() {
  var oTable = $("#datatable-editable-roomtype").dataTable({
    "sPaginationType": "full_numbers",
    aoColumnDefs: [
      {
        bSortable: false,
        aTargets: [-2, -1]
      }
    ]
  });
  var nEditing = null;

  $('#add-row').click( function (e) {
    e.preventDefault();

    var aiNew = oTable.fnAddData( [ '', '', '', '', '','',
      '<a class="edit-row" href="javascript:void(0)">Edit</a>', '<a class="delete-row" href="javascript:void(0)">Delete</a>' ] );
    var nRow = oTable.fnGetNodes( aiNew[0] );
    editRow( oTable, nRow );
    nEditing = nRow;
  } );

  // delete data from db
    var _nRow = 0;
    $('#remove').click(function (e) {
        e.preventDefault();
        var id = $(_nRow).data('roomid');

        $.ajax({
            url: site_url+"/roomtype_controller/delete",
            data: {
                id: id
            },
            type: 'post',
            dataType: 'html'
        }).done(function (data) {
            oTable.fnDeleteRow(_nRow);

            $('#confirm-delete').modal('hide');
            console.log('Deleted!!');

        });
        console.log('id: ',id);
    });

  $('#datatable-editable-roomtype').on('click', 'a.delete-row', function (e) {
    e.preventDefault();

    var nRow = $(this).parents('tr')[0];
    _nRow = nRow;
    //oTable.fnDeleteRow( nRow );

    $('#confirm-delete').modal('show');
  } );

  $('#datatable-editable-roomtype').on('click', 'a.edit-row', function (e) {
    e.preventDefault();

    /* Get the row as a parent of the link that was clicked on */
    var nRow = $(this).parents('tr')[0];

    if ( nEditing !== null && nEditing != nRow ) {
      /* Currently editing - but not this row - restore the old before continuing to edit mode */
      restoreRow( oTable, nEditing );
      editRow( oTable, nRow );
      nEditing = nRow;
    }
    else if ( nEditing == nRow && this.innerHTML == "Save" ) {
      /* Editing this row and want to save it */
      saveRow( oTable, nEditing );
      nEditing = null;
    }
    else {
      /* No edit in progress - let's start one */
      editRow( oTable, nRow );
      nEditing = nRow;
    }
  } );
} );
