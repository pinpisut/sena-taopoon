function restoreRow ( oTable, nRow )
{
  var aData = oTable.fnGetData(nRow);
  var jqTds = $('>td', nRow);
  
  for ( var i=0, iLen=jqTds.length ; i<iLen ; i++ ) {
    oTable.fnUpdate( aData[i], nRow, i, false );
  }
  
  oTable.fnDraw();
}

function editRow ( oTable, nRow )
{
  var aData = oTable.fnGetData(nRow);
  var jqTds = $('>td', nRow);

  
  var btn_img = $(nRow).find('.image-gallery');
  btn_img.show();

  var btn = btn_img.attr('id');
  var id = $(nRow).data('id');
  var w = btn_img.data('width');
  var h = btn_img.data('height');

  var btn_temp = $(nRow).find('.thumbnail-gallery');
  btn_temp.show();
  console.log(btn_temp);

  var btn2 = btn_temp.attr('id');
  var id2 = $(nRow).data('id');
  var w2 = btn_temp.data('width');
  var h2 = btn_temp.data('height');

  var galletyTypeText = aData[0];
  console.log(galletyTypeText);

  // Start Dropdown Function
  var galletyTypeText = aData[0];
  var dropDown_func = '<select class="form-control pull-left" id="galleryImg" name="galleryImg" style="width:140px; margin-right:8px;">';
        // $.each(list_func, function(index, val){
        //         dropDown_func += '<option value="'+val.id+'" '+ ((val.detail == funcOrgValue) ? 'selected' : '') +'>'+val.detail+'</option>';
        //    });
        dropDown_func += '<option value="Gallary"'+(("Gallary" == galletyTypeText) ? 'selected' : '')+'>'+'Gallary</option>';
        // dropDown_func += '<option value="Funiture"'+(("Funiture" == galletyTypeText) ? 'selected' : '')+'>'+'Funiture</option>';
      dropDown_func += '</select>';
  // End Drowdown Function

  uploadImages(btn, id, w, h, 'img');
  uploadImages(btn2, id2, w2, h2, 'temp');

  jqTds[0].innerHTML = dropDown_func;//'<input type="text" id="type" value="'+aData[0]+'">';
   // jqTds[1].innerHTML = '<input type="text" value="'+aData[1]+'">';
   // jqTds[2].innerHTML = '<input type="text" value="'+aData[2]+'">';
  jqTds[3].innerHTML = '<input type="text" id="name" value="'+aData[3]+'">';
  // jqTds[4].innerHTML = '<input type="text" id="instanceName" value="'+aData[4]+'">';
  jqTds[4].innerHTML = '<a class="edit-row" href="javascript:void(0)">Save</a>';
}

function saveRow ( oTable, nRow )
{
  var _type = $(nRow).find('#type');
  var _galleryimg = $(nRow).find('#galleryImg');
  var _name = $(nRow).find('#name');
  var _instanceName = $(nRow).find('#instanceName');

  var btn_temp = $(nRow).find('.thumbnail-gallery');
  btn_temp.hide();
  var btn_img = $(nRow).find('.image-gallery');
  btn_img.hide();

  console.log("_type: ", _type.val());
  console.log("_galleryimg: ", _galleryimg.val());
  console.log("_name", _name.val());
  console.log("_instanceName", _instanceName.val());

  //var jqInputs = $('input', nRow);
  oTable.fnUpdate( _galleryimg.val(), nRow, 0, false );
  //oTable.fnUpdate( jqInputs[1].value, nRow, 1, false );
  //oTable.fnUpdate( jqInputs[2].value, nRow, 2, false );
  oTable.fnUpdate( _name.val(), nRow, 3, false );
  // oTable.fnUpdate( _instanceName.val(), nRow, 4, false );
  oTable.fnUpdate( '<a class="edit-row" href="javascript:void(0)">Edit</a>', nRow, 4, false );
  oTable.fnDraw();

  var id = $(nRow).data('id');
    if (!$(nRow).data('id')) {
        id = 0;
    }
  console.log('id: ',id);

  $.ajax({
        url: site_url+"/gallery_controller/updateData",
        data: {
            id: id,
            type: _galleryimg.val(),
            name: _name.val(),
            // instance_name: _instanceName.val()
        },
        type: 'post',
        dataType: 'json'
    });/*.done(function (data) {
        if (id == 0) {
            $(nRow).data('id', data.last_id);
        }
    });*/

}

function uploadImages(btn, id, w, h, type){

    new AjaxUpload(btn, {
            action: url,
            name: 'userfile',
            data: {
                'id' : id,
                'width' : w,
                'height' : h,
                'type' : type
            },
            responseType: 'json',
            onSubmit: function(file, ext) {
                if (! (ext && /^(jpg|JPG|png|PNG|jpeg|JPEG|gif|GIF)$/.test(ext))){ 
                    // extension is not allowed
                    alert('Only JPG, PNG or GIF files are allowed');
                    return false;
                }
            },
            onComplete: function(file, response) {
                $('#'+btn).parent().find('img').attr('src',response.img_path);
            
                // this.setData({
                //     'id' : response.image_id,
                //     'order' : response.order,
                //     'image_name' : response.image,
                //     'category' : '<?php echo $category; ?>',
                //     'content_id' : '<?php echo $content_id; ?>',
                //     'content_type' : '<?php echo $content_type; ?>'
                // });
            }
        });
}

$(document).ready(function() {
  var oTable = $("#datatable-editable-gallery").dataTable({
    "sPaginationType": "full_numbers",
    aoColumnDefs: [
      {
        bSortable: false,
        aTargets: [-2, -1]
      }
    ]
  });
  var nEditing = null;

  $('#add-row').click( function (e) {
    e.preventDefault();

    var aiNew = oTable.fnAddData( [ '', '', '', '',
      '<a class="edit-row" href="javascript:void(0)">Edit</a>', '<a class="delete-row" href="javascript:void(0)">Delete</a>' ] );
    var nRow = oTable.fnGetNodes( aiNew[0] );
    editRow( oTable, nRow );
    nEditing = nRow;
  } );

  // delete data from db
    var _nRow = 0;
    $('#remove').click(function (e) {
        e.preventDefault();
        var id = $(_nRow).data('id');

        $.ajax({
            url: site_url+"/gallery_controller/delete",
            data: {
                id: id
            },
            type: 'post',
            dataType: 'html'
        }).done(function (data) {
            oTable.fnDeleteRow(_nRow);

            $('#confirm-delete').modal('hide');
            console.log('Deleted!!');

        });
        console.log('id: ',id);
    });

  $('#datatable-editable-gallery').on('click', 'a.delete-row', function (e) {
    e.preventDefault();

    var nRow = $(this).parents('tr')[0];
    _nRow = nRow;
    //oTable.fnDeleteRow( nRow );

    $('#confirm-delete').modal('show');
  } );

  $('#datatable-editable-gallery').on('click', 'a.edit-row', function (e) {
    e.preventDefault();

    /* Get the row as a parent of the link that was clicked on */
    var nRow = $(this).parents('tr')[0];

    if ( nEditing !== null && nEditing != nRow ) {
      /* Currently editing - but not this row - restore the old before continuing to edit mode */
      restoreRow( oTable, nEditing );
      editRow( oTable, nRow );
      nEditing = nRow;
    }
    else if ( nEditing == nRow && this.innerHTML == "Save" ) {
      /* Editing this row and want to save it */
      saveRow( oTable, nEditing );
      nEditing = null;
    }
    else {
      /* No edit in progress - let's start one */
      editRow( oTable, nRow );
      nEditing = nRow;
    }
  } );
} );
