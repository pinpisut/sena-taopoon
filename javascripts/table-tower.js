function restoreRow ( oTable, nRow )
{
  var aData = oTable.fnGetData(nRow);
  var jqTds = $('>td', nRow);
  
  for ( var i=0, iLen=jqTds.length ; i<iLen ; i++ ) {
    oTable.fnUpdate( aData[i], nRow, i, false );
  }
  
  oTable.fnDraw();
}

function editRow ( oTable, nRow )
{
  var aData = oTable.fnGetData(nRow);
  var jqTds = $('>td', nRow);
  jqTds[0].innerHTML = '<input type="text" value="'+aData[0]+'">';
  jqTds[1].innerHTML = '<a class="edit-row" href="javascript:void(0)">Save</a>';
}

function saveRow ( oTable, nRow )
{
  var jqInputs = $('input', nRow);
  oTable.fnUpdate( jqInputs[0].value, nRow, 0, false );
  oTable.fnUpdate( '<a class="edit-row" href="javascript:void(0)">Edit</a>', nRow, 1, false );
  oTable.fnDraw();

  var id = $(nRow).data('towerid');
  console.log('id : ',id);

  if(!$(nRow).data('towerid')){
    id = 0;
  }

  $.ajax( {
        url : "insertTower",
        data: {
          name: jqInputs[0].value,
          id: id
        },
        type:'post',
        dataType : 'html'
      }).done(function(data){
          if(id == 0){
            $(nRow).data('towerid', data.last_id);
          }
          console.log('Save Tower Complete!!');
          console.log(data);
      });
}

$(document).ready(function() {
  var oTable = $("#datatable-editable-tower").dataTable({
    "sPaginationType": "full_numbers",
    aoColumnDefs: [
      {
        bSortable: false,
        aTargets: [-2, -1]
      }
    ]
  });
  var nEditing = null;

  $('#add-row').click( function (e) {
    e.preventDefault();

    var aiNew = oTable.fnAddData( [ '',
      '<a class="edit-row" href="javascript:void(0)">Edit</a>', '<a class="delete-row" href="javascript:void(0)">Delete</a>' ] );
    var nRow = oTable.fnGetNodes( aiNew[0] );
    editRow( oTable, nRow );
    nEditing = nRow;
  } );

  // delete data from db
  var _nRow = 0;
  $('#remove').click(function(e){
    e.preventDefault();
      var tower_id = $(_nRow).data('towerid');
      
      $.ajax( {
          url : "deleteTower",
          data: {
            id : tower_id
          },
          type:'post',
          dataType : 'html'
        }).done(function(data){
            console.log('delete complete!!');
        
          oTable.fnDeleteRow( _nRow );

              $('#confirm-delete').modal('hide');
              console.log('Deleted!!');

      });
      console.log(tower_id);
  });

/*  $('#datatable-editable-tower').on('click', 'a.delete-row', function (e) {
    e.preventDefault();

    var nRow = $(this).parents('tr')[0];
    oTable.fnDeleteRow( nRow );
  } );*/

  $('.confirm-delete').click(function(e){
    console.log('delete naaaa');
  });

  $('#datatable-editable-tower').on('click', 'a.delete-row', function (e) {
    e.preventDefault();
    console.log('click!!!');

    var nRow = $(this).parents('tr')[0];
    _nRow = nRow;


    var id = $(nRow).data('towerid');
    $('#confirm-delete').modal('show');
  });


  $('#datatable-editable-tower').on('click', 'a.edit-row', function (e) {
    e.preventDefault();

    /* Get the row as a parent of the link that was clicked on */
    var nRow = $(this).parents('tr')[0];

    if ( nEditing !== null && nEditing != nRow ) {
      /* Currently editing - but not this row - restore the old before continuing to edit mode */
      restoreRow( oTable, nEditing );
      editRow( oTable, nRow );
      nEditing = nRow;
    }
    else if ( nEditing == nRow && this.innerHTML == "Save" ) {
      /* Editing this row and want to save it */
      saveRow( oTable, nEditing );
      nEditing = null;
    }
    else {
      /* No edit in progress - let's start one */
      editRow( oTable, nRow );
      nEditing = nRow;
    }
  } );
} );
